﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Matrix</title>
    <link rel="icon" href="images/favicon.ico" />
    <link href="CSS/font-awesome.min.css" rel="stylesheet" />
    <link href="CSS/angular-motion.min.css" rel="stylesheet" />
    <link href="CSS/bootstrap.min.css" rel="stylesheet" />
    <link href="CSS/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="CSS/css.css" rel="stylesheet" />
    <link href="CSS/fullcalendar.min.css" rel="stylesheet" />
    <link href="CSS/isteven-multi-select.css" rel="stylesheet" />
    <link href="CSS/textAngular.css" rel="stylesheet" />
    <link href="CSS/tooltipster.css" rel="stylesheet" />
    <link href="CSS/angular-chart.min.css" rel="stylesheet" />
    <link href="CSS/introjs.min.css" rel="stylesheet" />
</head>
<body ng-app="green" ng-controller="mainController">
    <form id="form1" runat="server">

        <nav class="navbar navbar-inverse  navbar-fixed-top">
            <div class="container" ng-show="user" data-step="1" data-intro="Hello, here is main navigation." data-position='bottom-middle-aligned'>
                <div class="navbar-header">
                    <div class="navbar-collapse collapse" id="navbar-main">
                        <ul class="nav navbar-nav">
                            <li><a style="font-weight: bold; font-size: 15px;" ng-model="selectedpagehere">{{selectedpagehere}}</a></li>
                        </ul>

                        <ul class="nav navbar-nav">
                            <a class="navbar-brand" ng-href="{{isManager ? '#/' : '#/companyDirectory'}}">
                                <%--<label>ETC</label>--%>
                                <%--<span class="glyphicon glyphicon-home"></span>--%>
                                <img class="brandImage" src="images/favicon.ico" />
                            </a>
                        </ul>
                    </div>

                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-main">

                    <ul class="nav navbar-nav">
                        <li ng-show="isLinkVisible.dashBoard" ng-class="{ active: selectedpagehere === 'Dashboard'}"><a href="#/" >Dashboard</a></li>

                        <li ng-hide="(!isLinkVisible.companyDirectory && !isLinkVisible.employeeProfile && !isLinkVisible.calendar && !isLinkVisible.employeePortal)" class="dropdown" ng-class="{ active: selectedPageDropDown === 'Human Resources'}">
                            <a bs-dropdown>Human Resources<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li ng-show="isLinkVisible.companyDirectory" ng-class="{ active: selectedpagehere === 'Company Directory'}"><a href="#/companyDirectory">Company Directory</a></li>
                                <li ng-show="isLinkVisible.employeeProfile" ng-class="{ active: selectedpagehere === 'Employee Profile'}"><a href="#/employeeProfile">Employee Profile</a></li>
                                <li ng-show="isLinkVisible.calendar" ng-class="{ active: selectedpagehere === 'Calendar'}"><a href="#/calendar">Calendar</a></li>
                                <li ng-show="isLinkVisible.employeePortal"><a target="_blank" href="https://secure2.saashr.com/ta/ETCGlobal.login">Employee Portal</a></li>
                            </ul>
                        </li>

                        <li ng-hide="(!isLinkVisible.boardStatistics && !isLinkVisible.monthlyClearing && !isLinkVisible.salesCommission && !isLinkVisible.volumesAndChargesStats)" class="dropdown" ng-class="{ active: selectedPageDropDown === 'Board Report'}">
                            <a bs-dropdown>Board Report<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li ng-show="isLinkVisible.boardStatistics" ng-class="{ active: selectedpagehere === 'Board Statistics'}"><a href="#/boardStatistics">Board Statistics</a></li>
                                <li ng-show="isLinkVisible.monthlyClearing" ng-class="{ active: selectedpagehere === 'Monthly Clearing'}"><a href="#/monthlyClearing">Monthly Clearing</a></li>
                                <li ng-show="isLinkVisible.salesCommission" ng-class="{ active: selectedpagehere === 'Sales Commission'}"><a href="#/salesCommission">Sales Commission</a></li>
                                <li ng-show="isLinkVisible.volumesAndChargesStats" ng-class="{ active: selectedpagehere === 'Volumes And Charges Stats'}"}"><a href="#/volumesAndChargesStats">Volumes And Charges Stats</a></li>
                                <li ng-show="isLinkVisible.clientRetention" ng-class="{ active: selectedpagehere === 'Client Retention'}"><a href="#/clientRetention">Client Retention</a></li>
                                <li ng-show="isLinkVisible.actualVsProjPipelineRev" ng-class="{ active: selectedpagehere === 'Actual vs. Proj. Pipeline Rev.'}"><a href="#/actualVsProjPipelineRev">Actual vs. Proj. Pipeline Rev.</a></li>
                           
                                 </ul>
                        </li>
                        <li ng-hide="(!isLinkVisible.clearingFeePerAccount && !isLinkVisible.monthlyClearingValidation && !isLinkVisible.yearlyRevenueAndShare)" class="dropdown" ng-class="{ active: selectedPageDropDown === 'Internal Control'}">
                            <a bs-dropdown>Internal Control<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li ng-show="isLinkVisible.clearingFeePerAccount" ng-class="{ active: selectedpagehere === 'Clearing Fee Per Account'}"><a href="#/clearingFeePerAccount">Clearing Fee Per Account</a></li>
                                <li ng-show="isLinkVisible.monthlyClearingValidation" ng-class="{ active: selectedpagehere === 'Monthly Clearing Validation'}"><a href="#/monthlyClearingValidation">Monthly Clearing Validation</a></li>
                                <li ng-show="isLinkVisible.yearlyRevenueAndShare" ng-class="{ active: selectedpagehere === 'Yearly Revenue And Sale'}"><a href="#/yearlyRevenueAndSale">Yearly Revenue And Sale</a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a>
                                <label>{{user}}</label>
                            </a>
                        </li>
                        <li><a ng-click="logOut()">Log Out</a></li>
                        <li><a id="start" onclick="tour();"><span class="glyphicon glyphicon-question-sign"></span></a></li>
                    </ul>


                </div>
            </div>
        </nav>

        <div id="page" class="container-fluid">
            <ng-view></ng-view>
        </div>


        <div class="footer">
            <div class="container ">

                <label class="pull-right">©<span>{{year}}</span> Electronic Transaction Clearing, Inc.</label>
            </div>
        </div>

    </form>

    <%--<script src="Scripts/moment.min.js"></script>--%>
    <script src="Scripts/moment.js"></script>
    <script src="Scripts/jquery-2.1.4.min.js"></script>
    <%--    <script src="Scripts/jquery-1.12.0.min.js"></script>--%>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/angular.min.js"></script>
    <script src="Scripts/angular-strap.min.js"></script>
    <script src="Scripts/angular-strap.tpl.min.js"></script>
    <script src="Scripts/angular-route.min.js"></script>
    <script src="Scripts/angular-animate.min.js"></script>
    <script src="Scripts/angular-local-storage.min.js"></script>
    <script src="Scripts/paging.min.js"></script>
    <script src="Scripts/fullcalendar.min.js"></script>
    <script src="Scripts/isteven-multi-select.js"></script>
    <script src="Scripts/jquery.tooltipster.min.js"></script>
    <script src="Scripts/RichText/textAngular-rangy.min.js"></script>
    <script src="Scripts/RichText/textAngular-sanitize.min.js"></script>
    <script src="Scripts/RichText/textAngular.min.js"></script>
    <script src="Scripts/Chart.min.js"></script>
    <script src="Scripts/angular-chart.min.js"></script>
    <script src="Scripts/intro.min.js"></script>
    <script src="Scripts/angular-intro.min.js"></script>
    <script src="Scripts/FileSaver.js"></script>
    

    <script src="app/scripts/app.js"></script>
    <script src="app/scripts/services.js"></script>
    <script src="app/scripts/mainController.js"></script>
    <script src="app/scripts/companyDirectoryController.js"></script>
    <script src="app/scripts/employeeProfile.js"></script>
    <script src="app/scripts/boardstatisticsController.js"></script>
    <script src="app/scripts/monthlyClearing.js"></script>
    <script src="app/scripts/salesCommission.js"></script>
    <script src="app/scripts/volumesAndChargesStats.js"></script>
    <script src="app/scripts/clearingFeePerAccount.js"></script>
    <script src="app/scripts/monthlyClearingValidation.js"></script>
    <script src="app/scripts/yearlyRevenueAndSale.js"></script>
    <script src="app/scripts/calendarController.js"></script>
    <script src="app/scripts/modalCalendarController.js"></script>
    <script src="app/scripts/modalEmployeeController.js"></script>
    <script src="app/scripts/dashboard.js"></script>
    <script src="app/scripts/loginController.js"></script>
    <script src="app/scripts/clientRetentionController.js"></script>
    <script src="app/scripts/actualVsProjPipelineRev.js"></script>
</body>
</html>
