﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Script.Serialization

Namespace MatrixClear

    Partial Public Class Mod_Class
        Implements IDisposable

        Dim objComm, objComm2 As New SqlCommand
        Dim objDSource As New SqlDataSource
        Dim objSQLConn, objSQLConn2 As New SqlConnection ' Matrix Web

        Dim objDA, objDA2 As New SqlDataAdapter
        Dim objDR As SqlDataReader
        Dim objDS As New DataSet
        Dim objDT As New DataTable


        Public Function SQLConnect(ByVal strDatabase As String) As SqlConnection 'Matrix Web
            Dim objConn As New SqlConnection
            Dim DBServer As String
            Dim UserName As String
            Dim Password As String


            'DBServer = "HQDEV01\MSSQL2010"
            'DBServer = "HQSQL02"
            DBServer = "HQSQLDEV02"
            'DBServer = "HQSQL03"
            'DBServer = "HQDEV01\MSSQL2010"
            'DBServer = "HQSQL03"
            'DBServer = "hqsqldev02\hqsqldev003"
            UserName = "trinity"
            Password = "Ann@1983"

            'use this on deployment
            objConn.ConnectionString = "user id=" & UserName & "; password=" & Password & "; Initial Catalog=" & strDatabase & "; Server=" & DBServer & "; Connect Timeout=10000;"

            'temporary connection string
            'objConn.ConnectionString = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString

            Try
                objConn.Open()
            Catch ex As Exception

            End Try

            Return objConn

            CloseClass()
            Dispose()
        End Function

        Function GetJson(ByVal sSQLString As String, ByVal strDBName As String) As String
            'original etc intranet
            If objSQLConn.State <> ConnectionState.Closed Then
                objSQLConn.Close()
            End If
            objSQLConn = SQLConnect(strDBName)
            objComm.CommandType = CommandType.Text
            objComm.CommandText = sSQLString.Trim
            objComm.Connection = objSQLConn
            objComm.CommandTimeout = 500
            objDA.SelectCommand = objComm
            objDA.Fill(objDT)
            Dim serializer = New JavaScriptSerializer()
            serializer.MaxJsonLength = Int32.MaxValue
            Dim data = serializer.Serialize(From dr As DataRow In objDT.Rows Select objDT.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
            CloseClass()
            Dispose()

            Return data
        End Function

        Sub ExecuteSQL(ByVal sSQLString As String, ByVal strDBName As String)
            'before parameter was a listbox 
            If objSQLConn.State <> ConnectionState.Closed Then
                objSQLConn.Close()
            End If
            objSQLConn = SQLConnect(strDBName)
            objComm.CommandType = CommandType.Text
            objComm.CommandText = sSQLString
            objComm.Connection = objSQLConn
            objComm.CommandTimeout = 0
            objDR = objComm.ExecuteReader(CommandBehavior.CloseConnection)
            CloseClass()
            Dispose()
        End Sub

        Function GetReader(ByVal sSQLString As String, ByVal strDBName As String) As SqlDataReader
            'before parameter was a listbox 
            If objSQLConn.State <> ConnectionState.Closed Then
                objSQLConn.Close()
            End If
            objSQLConn = SQLConnect(strDBName)
            objComm.CommandType = CommandType.Text
            objComm.CommandText = sSQLString
            objComm.CommandTimeout = 0
            objComm.Connection = objSQLConn
            objDR = objComm.ExecuteReader(CommandBehavior.CloseConnection)

            Return objDR
            CloseClass()
            Dispose()
        End Function
        Public Sub Dispose() Implements System.IDisposable.Dispose
            If objSQLConn.State <> ConnectionState.Closed Then objSQLConn.Close()

            objSQLConn.Dispose()

            objDS.Dispose()
            objDA.Dispose()
            objDA2.Dispose()
            objComm.Dispose()
            objComm2.Dispose()
            objDR = Nothing
        End Sub



        Sub CloseClass()
            Try
                If objSQLConn.State = ConnectionState.Open Then
                    objSQLConn.Close()
                    objSQLConn.Dispose()
                End If

                If Not objDS Is Nothing Then
                    objDS.Clear()
                    objDS.Dispose()
                End If

                If Not objComm Is Nothing Then
                    objComm.Dispose()
                End If

                If Not objComm2 Is Nothing Then
                    objComm2.Dispose()
                End If

                If Not objDR Is Nothing Then

                    objDR.Close()
                End If

            Catch ex As Exception

            End Try
        End Sub

    End Class

End Namespace
