﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net
Imports MatrixClear


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class ClientWebService
    Inherits System.Web.Services.WebService

    Dim objDR As SqlDataReader
    Dim mod_Class As New Mod_Class
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function LogInByFullName(ByVal FullName As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='LogInByFullName', @Name='" & FullName & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function UserPagesAccess(ByVal EmployeeID As Integer) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='UserPagesAccess', @EmployeeID=" & EmployeeID, "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetDate() As String
        Return DateTime.Now.ToString("MM-dd-yyyy")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetDepartments() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='Get Department'", "Matrix")
    End Function

    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetCompanyDirectory(ByVal Department As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='Get Company Directory', @Department='" & Department & "'", "Matrix")
    End Function

    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetEmployee(ByVal EmployeeID As Integer) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='Get Employee', @EmployeeID='" & EmployeeID & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetEmployees(ByVal Department As String, ByVal Status As Boolean) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='Get Employees', @Status='" & IIf(Status, "Active", "") & "', @Department='" & Department & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetEmployeeNames() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Employee Names'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetDivisions() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Divisions'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetYears() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetYears'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetBoardStatistics(ByVal Month As String, ByVal Year As String, ByVal Unit As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Board Statistics', @Month = '" & Month & "' ,@Year = '" & Year & "',@Unit = '" & Unit & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ProcessBoardStatistics(ByVal Month As String, ByVal Year As String, ByVal Unit As String) As String
        'to be check on server
        Return mod_Class.GetJson("EXEC RPT.BoardStatistics_SP @Month = '" & Month & "',@Year = '" & Year & "',@CompanyName = '" & Unit & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetMonthlyClearing(ByVal Month As String, ByVal Year As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetMonthlyClearing',@Month='" & Month & "',@Year='" & Year & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetSalesRep() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetSalesRep'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetSalesCommissionSummary(ByVal Month As String, ByVal Year As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetSalesCommissionSummary',@Month='" & Month & "',@Year='" & Year & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetSalesCommissionDetails(ByVal Month As String, ByVal Year As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetSalesCommissionDetails',@Month='" & Month & "',@Year='" & Year & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetCompanies() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetCompanies'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetEmployeeCompanies() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Employee Companies'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetEmploymentStatuses() As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Employment Statuses'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetVolumesChargesAndStats(ByVal Company As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Office = '" & Company & "',@Action = 'Get Volumes Charges And Stats'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Sub SalesCommissionRefreshReport(ByVal Month As String, ByVal Year As String)
        mod_Class.ExecuteSQL("EXEC RPT.SalesCommissions_SP @Month = '" & Month & "', @Year = '" & Year & "'", "Matrix")
        mod_Class.ExecuteSQL("EXEC MTX.Executables_sp @ExecName = 'Sales Commission' ,@Month = '" & Month & "', @Year = '" & Year & "'", "Matrix")
    End Sub
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetRollUp(ByVal Month As String, ByVal Year As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetRollUp',@Month = '" & Month & "', @Year = '" & Year & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetClearingFeePerAccount(ByVal Month As String, ByVal Year As String, ByVal RollUp As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Clearing Fee Per Account',@Year = '" & Year & "',@Month = '" & Month & "',@RollUp = '" & RollUp & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetMonthlyClearingValidation(ByVal Month As String, ByVal Year As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Monthly Clearing Validation', @Month = '" & Month & "', @Year =  '" & Year & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetMonthlyChart(ByVal StartDate As Date, ByVal EndDate As Date) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Monthly Clearing Validation Chart',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ClientRetentionChart(ByVal StartDate As Date, ByVal EndDate As Date, ByVal Unit As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ClientRetentionChart',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "',@Unit =  '" & Unit & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ClientRetentionUnits(ByVal StartDate As Date, ByVal EndDate As Date) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ClientRetentionUnits',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ActualVsProjPipelineRevUnits(ByVal StartDate As Date, ByVal EndDate As Date) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ActualVsProjPipelineRevUnits',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ActualVsProjPipelineRevChart(ByVal StartDate As Date, ByVal EndDate As Date, ByVal Unit As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ActualVsProjPipelineRevChart',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "',@Unit =  '" & Unit & "'", "Matrix")
    End Function

    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ActualVsProjPipelineCompanyName(ByVal StartDate As Date, ByVal EndDate As Date) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ActualVsProjPipelineCompanyName',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetMonthYearBetween(ByVal StartDate As Date, ByVal EndDate As Date) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'GetMonthYearBetween',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ActualVsProjPipelineRevTable(ByVal StartDate As Date, ByVal EndDate As Date, ByVal Unit As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ActualVsProjPipelineRevTable',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "',@Unit =  '" & Unit & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function ClientRetentionTable(ByVal StartDate As Date, ByVal EndDate As Date, ByVal Unit As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'ClientRetentionTable',@StartDate = '" & StartDate & "',@EndDate =  '" & EndDate & "',@Unit =  '" & Unit & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetYearlyRevenueAndSale(ByVal Year As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Yearly Revenue And Sale', @Year = '" & Year & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetCalendar(ByVal StartDate As String, ByVal EndDate As String, ByVal Type As String) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'Get Calendar',@StartDate = '" & StartDate & "',@EndDate = '" & EndDate & "',@Type = '" & Type & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function AddOrEditEmployee(ByVal EmployeeID As String,
                                                                                                ByVal Division As String,
                                                                                                ByVal Department As String,
                                                                                                ByVal FirstName As String,
                                                                                                ByVal LastName As String,
                                                                                                ByVal Title As String,
                                                                                                ByVal CellPhone As String,
                                                                                                ByVal WorkPhone As String,
                                                                                                ByVal HomePhone As String,
                                                                                                ByVal EmailAdd As String,
                                                                                                ByVal AltEmailAdd As String,
                                                                                                ByVal AIMID As String,
                                                                                                ByVal Manager As String,
                                                                                                ByVal Status As String,
                                                                                                ByVal CompanyName As String,
                                                                                                ByVal EmploymentStatus As String,
                                                                                                ByVal DivisionManager As String,
                                                                                                ByVal HireDate As Date,
                                                                                                ByVal ModifiedBy As String,
                                                                                                ByVal SkypeID As String) As String

        Return mod_Class.GetJson("EXEC ETC.EmployeeProfile_sp @Action='" & IIf(EmployeeID, "Update Summary", "Insert") & "',@EmployeeID='" & EmployeeID &
                                 "',@Division='" & Division &
                                 "',@Department='" & Department &
                                 "',@FirstName='" & FirstName &
                                 "',@LastName='" & LastName &
                                 "',@Title='" & Title &
                                 "',@CellPhone='" & CellPhone &
                                 "',@WorkPhone='" & WorkPhone &
                                 "',@HomePhone='" & HomePhone &
                                 "',@EmailAdd='" & EmailAdd &
                                 "',@AltEmailAdd='" & AltEmailAdd &
                                 "',@AIMID='" & AIMID &
                                 "',@Manager='" & Manager &
                                 "',@Status='" & Status &
                                 "',@CompanyName='" & CompanyName &
                                 "',@EmploymentStatus='" & EmploymentStatus &
                                 "',@DivisionManager=" & DivisionManager &
                                 ",@HireDate=" & IIf(HireDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & HireDate & "'") &
                                 ",@ModifiedBy='" & ModifiedBy &
                                 "', @SkypeID='" & SkypeID & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function DeleteEmployee(ByVal EmployeeID As Integer) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action = 'DeleteEmployee', @EmployeeID = '" & EmployeeID & "'", "Matrix")
    End Function

    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function DownloadCompanyDirectoryPage() As String
        Dim FileName = "", FilePath As String
        FilePath = ""

        'mod_Class.ExecuteSQL("EXEC MTX.Executables_sp @ExecName = 'Company Directory Page'", "Matrix")
        objDR = mod_Class.GetReader("EXEC MTX.Matrix_SP @Action = 'DownloadCompanyDirectoryPage'", "Matrix")

        If objDR.Read = True And objDR.HasRows = True Then
            FilePath = IIf(IsDBNull(objDR!FilePath), "", objDR!FilePath)
            FileName = IIf(IsDBNull(objDR!FileName), "", objDR!FileName)
        End If

        FilePath = FilePath & FileName

        objDR.Close()
        mod_Class.Dispose()
        Return "CompanyDirectory\" & FileName
    End Function

    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function AddOrEditCalendarAnnouncement(ByVal CalendarID As String,
                                                                                                ByVal Type As String,
                                                                                                ByVal FromDate As Date,
                                                                                                ByVal ToDate As Date,
                                                                                                ByVal Title As String,
                                                                                                ByVal Announcement As String,
                                                                                                ByVal AttachedFile As String,
                                                                                                ByVal SendAnnouncement As String,
                                                                                                ByVal File As String
                                                                                               ) As String


        Dim strPath = ""

        'use filepath from database

        'Dim objDR As SqlDataReader
        'objDR = mod_Class.GetReader("Exec GTS.Procedures_sp @Action='Get Attached file Path'", "GTS")
        'If objDR.Read = True And objDR.HasRows = True Then
        '    strPath = Replace(objDR.Item(0).ToString, "[GTSID]", GTSID & "\")
        'End If
        'objDR.Close()

        'use virtual directory filepath
        strPath = System.Web.Hosting.HostingEnvironment.MapPath("~/CalendarAttachments/" & Type + "/")

        If Directory.Exists(strPath) Then
        Else
            'Create folder
            Directory.CreateDirectory(strPath)
        End If

        Replace(AttachedFile, "&", "and")

        Dim binaryData() As Byte = Convert.FromBase64String(File)

        Dim fs As New FileStream(strPath & AttachedFile, FileMode.Create)

        fs.Write(binaryData, 0, binaryData.Length)

        fs.Close()
        fs.Dispose()
        ' return OK if we made it to here

        Return mod_Class.GetJson("Exec MTX.Matrix_SP @Title='" & Title & "', @Action='" & IIf(CalendarID, "AnnouncementEdit", "AnnouncementAdd") & "', @StartDate=" & IIf(FromDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & FromDate & "'") & ", @EndDate=" & IIf(ToDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & ToDate & "'") &
                                 ", @Note='" & Announcement &
                                 "', @Type='" & Type & "', @CalendarID='" & CalendarID & "', @File='" & Replace(AttachedFile, "'", "''") & "', @Send=" & SendAnnouncement, "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function AddOrEditCalendarAnnouncementNoUpload(ByVal CalendarID As String,
                                                                                                ByVal Type As String,
                                                                                                ByVal FromDate As Date,
                                                                                                ByVal ToDate As Date,
                                                                                                ByVal Title As String,
                                                                                                ByVal Announcement As String,
                                                                                                ByVal AttachedFile As String,
                                                                                                ByVal SendAnnouncement As String
                                                                                               ) As String


        Return mod_Class.GetJson("Exec MTX.Matrix_SP @Title='" & Title & "', @Action='" & IIf(CalendarID, "AnnouncementEdit", "AnnouncementAdd") & "', @StartDate=" & IIf(FromDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & FromDate & "'") & ", @EndDate=" & IIf(ToDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & ToDate & "'") &
                                 ", @Note='" & Announcement &
                                 "', @Type='" & Type & "', @CalendarID='" & CalendarID & "', @File='" & AttachedFile & "', @Send=" & SendAnnouncement, "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function AddOrEditCalendarHoliday(ByVal CalendarID As String,
                                                                                                ByVal Type As String,
                                                                                                ByVal FromDate As Date,
                                                                                                ByVal ToDate As Date,
                                                                                                ByVal Title As String,
                                                                                                ByVal Announcement As String,
                                                                                                ByVal AttachedFile As String,
                                                                                                ByVal SendAnnouncement As String,
                                                                                                ByVal NoTrade As String,
                                                                                                ByVal NoSettlement As String,
                                                                                                ByVal File As String
                                                                                               ) As String


        Dim strPath = ""

        'use filepath from database

        'Dim objDR As SqlDataReader
        'objDR = mod_Class.GetReader("Exec GTS.Procedures_sp @Action='Get Attached file Path'", "GTS")
        'If objDR.Read = True And objDR.HasRows = True Then
        '    strPath = Replace(objDR.Item(0).ToString, "[GTSID]", GTSID & "\")
        'End If
        'objDR.Close()

        'use virtual directory filepath
        strPath = System.Web.Hosting.HostingEnvironment.MapPath("~/CalendarAttachments/" & Type + "/")

        If Directory.Exists(strPath) Then
        Else
            'Create folder
            Directory.CreateDirectory(strPath)
        End If

        Replace(AttachedFile, "&", "and")

        Dim binaryData() As Byte = Convert.FromBase64String(File)

        Dim fs As New FileStream(strPath & AttachedFile, FileMode.Create)

        fs.Write(binaryData, 0, binaryData.Length)

        fs.Close()
        fs.Dispose()
        ' return OK if we made it to here

        Return mod_Class.GetJson("Exec MTX.Matrix_SP @Title='" & Title & "', @Action='" & IIf(CalendarID, "AnnouncementEdit", "AnnouncementAdd") & "', @StartDate=" & IIf(FromDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & FromDate & "'") & ", @EndDate=" & IIf(ToDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & ToDate & "'") &
                                 ", @Note='" & Announcement &
                                 "', @NoTrade='" & NoTrade & "', @CalendarID='" & CalendarID & "', @NoSettlement='" & NoSettlement & "', @Type='" & Type & "', @File='" & Replace(AttachedFile, "'", "''") & "', @Send=" & SendAnnouncement, "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function AddOrEditCalendarHolidayNoUpload(ByVal CalendarID As String,
                                                                                                ByVal Type As String,
                                                                                                ByVal FromDate As Date,
                                                                                                ByVal ToDate As Date,
                                                                                                ByVal Title As String,
                                                                                                ByVal Announcement As String,
                                                                                                ByVal AttachedFile As String,
                                                                                                ByVal SendAnnouncement As String,
                                                                                                ByVal NoTrade As String,
                                                                                                ByVal NoSettlement As String
                                                                                               ) As String


        Return mod_Class.GetJson("Exec MTX.Matrix_SP @Title='" & Title & "', @Action='" & IIf(CalendarID, "AnnouncementEdit", "AnnouncementAdd") & "', @StartDate=" & IIf(FromDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & FromDate & "'") & ", @EndDate=" & IIf(ToDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & ToDate & "'") &
                                 ", @Note='" & Announcement &
                                 "', @NoTrade='" & NoTrade & "', @CalendarID='" & CalendarID & "', @NoSettlement='" & NoSettlement & "', @Type='" & Type & "', @File='" & AttachedFile & "', @Send=" & SendAnnouncement, "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function AddOrEditCalendarRoomReserve(ByVal CalendarID As String,
                                                                                                ByVal Title As String,
                                                                                                ByVal Type As String,
                                                                                                ByVal FromDate As Date,
                                                                                                ByVal ToDate As Date,
                                                                                                ByVal Announcement As String,
                                                                                                ByVal AttachedFile As String
                                                                                               ) As String


        Return mod_Class.GetJson("Exec MTX.Matrix_SP  @Title='" & Title & "' , @Action='" & IIf(CalendarID, "AnnouncementEdit", "AnnouncementAdd") & "', @StartDate=" & IIf(FromDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & FromDate & "'") & ", @EndDate=" & IIf(ToDate.ToString() = "1/1/1900 12:00:00 AM", "null", "'" & ToDate & "'") &
                                 ", @Note='" & Announcement &
                                 "', @Type='" & Type & "', @CalendarID='" & CalendarID & "', @File='" & AttachedFile & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetCalendarModal(ByVal CalendarID As Integer) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='GetCalendarModal', @CalendarID='" & CalendarID & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function GetCalendarRoomModal(ByVal CalendarID As Integer) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='GetCalendarRoomModal', @CalendarID='" & CalendarID & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function DeleteCalendar(ByVal CalendarID As Integer) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='AnnouncementDel', @CalendarID='" & CalendarID & "'", "Matrix")
    End Function
    <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> Public Function TestReservation(ByVal CalendarID As Integer,
                                                                                                      ByVal FromDate As DateTime,
                                                                                                      ByVal ToDate As DateTime,
                                                                                                      ByVal AttachedFile As String
                                                                                                        ) As String
        Return mod_Class.GetJson("EXEC MTX.Matrix_SP @Action='TestReservation', @CalendarID ='" & CalendarID & "', @StartDate='" & FromDate & "',  @EndDate='" & ToDate & "', @File ='" & AttachedFile & "'", "Matrix")
    End Function
End Class



