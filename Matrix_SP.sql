USE [Matrix]
GO
/****** Object:  StoredProcedure [MTX].[Matrix_SP]    Script Date: 5/29/2016 12:16:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [MTX].[Matrix_SP]
       @Action VARCHAR(50) = ''
	   ,@Department VARCHAR(30) = ''
	   ,@Status VARCHAR(30) = 'Active'
	   ,@Month varchar(20) = ''
	   ,@Year char(4) = ''
	   ,@Unit varchar(300) = 'ALL'
	   ,@StartDate smalldatetime = Null
	   ,@EndDate smalldatetime = Null
	   ,@Type VARCHAR(30) = ''
	   ,@Office varchar(100) = ''
	   ,@SQL varchar(5500) = ''
	   ,@EmployeeID int = 0
	   ,@Note VARCHAR(max) = ''
	   ,@CalendarID bigint = 0
	   ,@Send bit = 1
	   ,@Title varchar(300)=''
	   ,@File varchar(500)=''
	   ,@NoTrade bit = 0
	   ,@NoSettlement bit = 0
	   ,@RollUp varchar(300)=''
	   ,@Name varchar(100)=''
AS

--------------------------------------------------
-- Start: Get SP log
--------------------------------------------------
Declare @SPName varchar(50), @Schema varchar(15) 
SET @SPName = OBJECT_NAME(@@PROCID)
SET @Schema = OBJECT_SCHEMA_NAME(@@PROCID) 
EXEC MTX.SPLog_SP @SPName,@Schema,'Start'

BEGIN

--------------------------------------------------
--------------------------------------------------
IF @Action='Get Department'
	BEGIN
		  SELECT 'All' AS [Name] UNION ALL
		  SELECT Distinct Code AS [Name]
		  FROM ADM.systemcode 
		  WHERE type ='Department' 
		  Order By [Name]
	END
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action='LogInByFullName'
	BEGIN
		  SELECT EmployeeID, [FirstName] + ' ' + [LastName] as FullName,[FirstName],[Division Manager] as isManager FROM Matrix.ETC.EmployeeProfile WHERE [FirstName] + ' ' + [LastName] = @Name
	END
ELSE IF @Action='UserPagesAccess'
	BEGIN
		  SELECT Name FROM ADM.Menu WHERE MenuID IN (SELECT MenuID FROM ADM.MenuAccess WHERE EmployeeID = @EmployeeID) AND Site = 'Matrix'
	END
ELSE IF @Action='GetMonthlyClearing'
	BEGIN
		  SELECT Title, Amount1, Amount2, Amount3 FROM RPT.MonthlyCharges  WHERE [Month] = @Month AND [Year] = @Year
	END
ELSE IF @Action='GetSalesRep'
	BEGIN
		  SELECT [Sales Rep] AS Name FROM ETC.SalesRep
	END
ELSE IF @Action='GetSalesCommissionSummary'
	BEGIN
		  SELECT Month + ' - ' + Year Period, ETC.FormatAccounting(SUM(Expense)) [TotalExpense]
		  , ETC.FormatAccounting(SUM([Revenue])) as [TotalRevenue], ETC.FormatAccounting(SUM([Net Revenue])) [TotalNetRevenue]
		  , Count(*) [ClientCount], ETC.FormatAccounting(Expense) [ExpensePerClient]
		  ,ETC.FormatPercentage((SUM([Sales (%)])/(Count(*) * 15)) * 100) [SalePercent],ETC.FormatAccounting(SUM([Sales ($)])) [SalesDollar]
		  , ETC.FormatPercentage((SUM([ETC (%)])/(Count(*) * 15)) * 100) [ETCPercent],ETC.FormatAccounting(SUM([ETC ($)])) [ETCDollar]
		  , ETC.FormatAccounting(SUM(EBITDA)) as EBITDA
		  , ETC.FormatAccounting(SUM([ETC Pool])) as [ETCPool] FROM RPT.ETCCompensation WHERE Month = @Month
		  AND Year = @Year GROUP BY Month + ' - ' + Year,Expense
	END
ELSE IF @Action='GetSalesCommissionDetails'
	BEGIN
		  SELECT Account,[Sales Rep][SalesRep],ETC.FormatAccounting(Revenue) Revenue
		  , ETC.FormatAccounting([Net Revenue]) [NetRevenue]
		  , ETC.FormatPercentage([Sales (%)]) [SalesPercent], ETC.FormatAccounting([Sales ($)]) [SalesDollar]
		  FROM RPT.ETCCompensation WHERE Month = @Month AND Year = @Year
	END
ELSE IF @Action='GetCompanies'
	BEGIN
		  SELECT '' Name Union SELECT Distinct [Company Name] Name FROM ETC.ClientAccount WHERE Correspondent <> '' AND Office <> 'ELE' AND Status = 'Active'
	END
ELSE IF @Action='GetRollUp'
	BEGIN
		  SELECT '' [Name] UNION ALL SELECT Distinct [Roll Up] Name FROM Fee.ClearingFeePerAccount A WHERE Month = @Month AND Year = @Year AND [Roll Up] IS NOT NULL Order By Name
	END
ELSE IF @Action='DeleteEmployee'
	BEGIN
		  Delete FROM ETC.EmployeeProfile WHERE EmployeeID = @EmployeeID
	END
ELSE IF @Action='DownloadCompanyDirectoryPage'
	BEGIN
		  SELECT [File Path] as FilePath , [File Name Format] as FileName FROM ADM.FilePath WHERE Type = 'Company Directory Page'
	END
	
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action= 'GetYears'
BEGIN
	with yearlist as 
	(
		select YEAR(getdate()) - 2 as year
		union all
		select yl.year + 1 as year
		from yearlist yl
		where yl.year + 1 <= YEAR(GetDate())
	)

	select year AS Name from yearlist order by year desc;
END
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action= 'Get Employee Companies'
BEGIN
      SELECT '' [Name] 
      UNION
      SELECT code [Name]
      FROM ADM.SystemCode
      WHERE Type = 'Company Name'
END
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Employment Statuses'
BEGIN
	  SELECT '' [Name]
	  union all
      SELECT Code [Name]
      FROM adm.systemcode  A
      WHERE type = 'Employment Status'
      Order by [Name]
END

--------------------------------------------------
--------------------------------------------------

ELSE IF @Action='Get Company Directory'
BEGIN
      SELECT  FirstName + ' ' + LastName as [Name],Status
      , [Company Name] [CompanyName],Title, Division , Department ,[Cell Phone] [CellPhone], [Work Phone] [WorkPhone], [AIM Add] [AIMAdd], [Email Add] [EmailAdd], [SkypeID]
      FROM ETC.EmployeeProfile
      WHERE Status = 'Active'
       AND (@Department = '' OR Department = @Department)
       AND LastName != 'Login'
      Order by [Company Name], FirstName, LastName
END

--------------------------------------------------
--------------------------------------------------
ELSE IF @Action='Get Employees'
BEGIN
      SELECT      EmployeeID, FirstName + ' ' + LastName as [Name]
      , [Company Name] [CompanyName],Title , Division,  Department ,[Cell Phone] [CellPhone],[Home Phone] [HomePhone], [AIM Add] [AIMID], [Employment Status] [EmploymentStatus],  [SkypeID]
      FROM ETC.EmployeeProfile
      WHERE Status = CASE @Status 
                                    WHEN 'Active' THEN @Status
                                    ELSE 'Inactive' 
                              END
      AND Department = CASE @Department
                                    WHEN 'ALL' THEN Department
                                    WHEN '' THEN Department
                                    ELSE @Department
                              END
      AND Title != 'Default Account'
      Order by Department, FirstName
END
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action='Get Divisions'
BEGIN
      SELECT Distinct Code [Name]
      FROM ADM.systemcode 
      WHERE type ='Division' 
      Order By Name
END
--------------------------------------------------
--------------------------------------------------
IF @Action='Get Employee'
BEGIN
      SELECT      Division,Department, FirstName,LastName,Title,[Cell Phone] [CellPhone]
                  ,[Work Phone] [WorkPhone],[Home Phone] [HomePhone],[Email Add] [EmailAdd],[Alt Email Add] [AltEmailAdd],[AIM Add] [AIMID], [Company Name] [CompanyName]
                  ,Manager,Status,UserName,[Employment Status] [EmploymentStatus], [Division Manager] [DivisionManager], ETC.FormatDates([Hire Date], 'MM/DD/YYYY') [HireDate],[SkypeID]
                 
      FROM ETC.EmployeeProfile
      WHERE EmployeeID=@EmployeeID
END
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action= 'Get Employee Names'
BEGIN
      SELECT '' Name 
      UNION
      SELECT FirstName + ' ' + LastName as Name
      FROM ETC.EmployeeProfile
      WHERE Title != 'Default Account'
      AND Status = 'Active'
      ORDER BY Name
END
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Board Statistics' 
Begin
	SELECT Period
	, Unit
	, ETC.FormatQuantity([No. of Clients]) [NoOfClients],
	ETC.FormatPercentage([Percent of Clients]) [PercentOfClients],

	ETC.FormatCurrency(Revenue) [Revenue],
    ETC.FormatPercentage([Percent Revenue]) [PercentRevenue]	,
	
	ETC.FormatQuantity([Eq. Volume]) [EqVolume],
	ETC.FormatPercentage([Eq. Percent Volume]) [EqPercentVolume],
	ETC.FormatQuantity([Eq. Maket Volume]) [EqMaketVolume],	
	ETC.FormatPercentage([Eq. ETC % Market Volume]) [EqETCPercentVolume],	
	
	ETC.FormatQuantity([Opt. Volume]) [OptVolume],
	ETC.FormatPercentage([Opt. Percent Volume]) [OptETCPercentVolume],
	ETC.FormatQuantity([Opt. Maket Volume]) [OptMaketVolume],
	ETC.FormatPercentage([Opt. ETC % Market Volume]) [OptPercentVolume]

	FROM RPT.BoardStatistics WITH (NOLOCK)
	Where Month = @Month
	And	 Year = @Year
	And [Company Name] =  @Unit 
	Order by ID
End
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Clearing Fee Per Account' 
Begin
	SELECT Correspondent
		, Office
		, [Account No] [AccountNo]
		, [Company Name] [CompanyName]
		, [Roll Up] [RollUp]
		, ETC.FormatQuantity([Share Count]) as [ShareCount]
		, STR([Rate],15,6) Rate
		, ETC.FormatCurrency([Clearing Fee]) [ClearingFee]
	FROM Fee.ClearingFeePerAccount A WHERE Month =  @Month AND Year = @Year AND [Roll Up] Like '%' + @RollUp + '%'
End

--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Monthly Clearing Validation'
Begin
	SELECT [Company Name] [CompanyName]
	,[Roll Up] [RollUp]
	,[Share Count] [ShareCount]
	,STR([Rate],15,9) Rate
	,[Charge Per Share] [ChargePerShare]
	,[Actual Charge] [ActualCharge]
	,[Fee Schedule] [FeeSchedule]
	,[Fee Matrix] [FeeMatrix]
	,Correspondent + '-' + Office + '-' + [Account No] + '-' + [Sub Account No] + '-' + [Account Type] as Account
	FROM FEE.MatrixClearingFeeDetails WHERE Month = @Month AND Year =  @Year
End

ELSE IF @Action = 'GetMonthYearBetween'
Begin
	SELECT  DATENAME(MONTH, DATEADD(MONTH, x.number, @StartDate)) + ' ' + DATENAME(Year, DATEADD(MONTH, x.number, @StartDate)) AS [Name]
		
	FROM    master.dbo.spt_values x
	WHERE   x.type = 'P'        
	AND     x.number <= DATEDIFF(MONTH, @StartDate, @EndDate);
End
----------------------------------------------------
----------------------------------------------------

ELSE IF @Action = 'ActualVsProjPipelineRevTable'
Begin

--	SELECT [Month] + ' ' + [Year] [Date], [Company Name] [CompanyName], [Client Type] [ClientType]

--	, [Actual Rev.] [ActualRev]
--	, Convert(decimal(18,2), 0) as [RollingActualRev]
	
--	, [Proj. Pipeline Rev.] [ProjPipelineRev]
--	, Convert(decimal(18,2), 0) as [RollingProjPipelineRev]

--	, [Actual vs. Proj. Pipeline Rev.] [ActualVsProjPipelineRev]
--	FROM RPT.[ActualVs.Proj.Pipeline]
--	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate and @EndDate
--	AND Unit = @Unit
--	Order By  Convert(date, [Month] + ' 01 ' + [Year]) , [Company Name]

	SELECT [Company Name] [CompanyName], [Client Type] [ClientType]
	, SUM([Actual Rev.]) as [ActualRev]
	, SUM([Proj. Pipeline Rev.]) as  [ProjPipelineRev]
	, SUM([Actual vs. Proj. Pipeline Rev.]) as [ActualVsProjPipelineRev]
	FROM RPT.[ActualVs.Proj.Pipeline]
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate and @EndDate
	AND Unit = @Unit
	GROUP BY [Company Name], [Client Type]
	Order By  [Company Name]
End
----------------------------------------------------
----------------------------------------------------

ELSE IF @Action = 'ActualVsProjPipelineRevChart'
Begin
	SELECT [Month] + ' ' + [Year] [Date]
	, SUM([Actual Rev.]) [ActualRev]
	, SUM([Proj. Pipeline Rev.]) [ProjPipelineRev]
	FROM RPT.[ActualVs.Proj.Pipeline]
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate and @EndDate
	AND Unit = @Unit
	GROUP BY Year, Month
	order by  Convert(date, [Month] + ' 01 ' + [Year])

End
--------------------------------------------------
--------------------------------------------------
ELSE If @Action = 'ActualVsProjPipelineRevUnits'
Begin
	SELECT Distinct [Unit] [value]
	FROM RPT.[ActualVs.Proj.Pipeline]
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate AND @EndDate
	Order By [Unit]
End
--------------------------------------------------
--------------------------------------------------

ElSE If @Action = 'ActualVsProjPipelineCompanyName'
Begin

-- SELECT Convert(date, @FromMonth + ' 01 ' + @FromYear) as [From Date] , Convert(date, @ToMonth + ' 01 ' + @ToYear) as [To Date]

	SELECT Distinct [Company Name] [value]
	FROM RPT.[ActualVs.Proj.Pipeline]
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate and @EndDate
	Order By [Company Name]
End
--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Monthly Clearing Validation Chart'
Begin
	SELECT Month + ' ' + Year [Date],SUM([Share Count]) [ShareCount],SUM([Actual Charge]) [ChatClearingRevenue]
	FROM FEE.MatrixClearingFeeDetails
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate and @EndDate
	--WHERE cast((Year + '-' + Month + '-1' ) AS Datetime) <= DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@EndDate)+1,0))
	--	AND (Year + '-' + Month + '-1' ) >= DATEADD(month, DATEDIFF(month, 0, @StartDate), 0)
	GROUP BY Year, Month ORDER BY Year, Month
End

ELSE IF @Action = 'ClientRetentionTable'
Begin
	SELECT [Month] + ' ' + [Year] [Date], [Company Name] CompanyName, Status
	FROM RPT.ClientRetention
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate AND @EndDate
	AND Unit = @Unit
	Order By  Convert(date, [Month] + ' 01 ' + [Year]), [Company Name]
End
ELSE If @Action = 'ClientRetentionUnits'
Begin
	SELECT Distinct [Unit] [value]
	FROM RPT.ClientRetention
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate AND @EndDate
	Order By [Unit]
End
ELSE IF @Action = 'ClientRetentionChart'
Begin
	SELECT[Month] + ' ' + [Year] [Date],COUNT([Status]) [Count],[Status]
	FROM RPT.ClientRetention
	WHERE Convert(date, [Month] + ' 01 ' + [Year]) Between @StartDate AND @EndDate
	AND Unit = @Unit
	GROUP BY [Status],Year, Month
	Order By  Convert(date, [Month] + ' 01 ' + [Year]), [Status]
End

--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Yearly Revenue And Sale'

BEGIN

	IF OBJECT_ID('tempdb..#ClientFeeTrns') IS NOT NULL DROP TABLE #ClientFeeTrns
	---------------------------------------------------------------------------
	--Gather Clearing fee details
	---------------------------------------------------------------------------
	SELECT	Replace([Roll up],',',' ') as Account,[Month],[Year]
			,SUM(ISNULL([Actual Charge],0)) [Actual Charge]
			,SUM(ISNULL([Share Count],0)) [Share Count]
	INTO #ClientFeeTrns
	FROM FEE.MatrixClearingFeeDetails
	WHERE Year = @Year
	AND [Roll up] IS NOT NULL
	GROUP BY [Roll up],[Month],[Year]


	UPDATE A SET Account = REPLACE(Account,' :',':')
	FROM #ClientFeeTrns A

	---------------------------------------------------------------------------
	--Pivot the share and revenue
	---------------------------------------------------------------------------
	IF OBJECT_ID('Tempdb..#Revenue') IS NOT NULL DROP TABLE #Revenue
	SELECT Account

		,	'' as [Agreement Terms]

		,	CONVERT(decimal(18,0),0) as [January Share Count]
		,	SUM([January]) [January]

		,	CONVERT(decimal(18,0),0) as [February Share Count]
		,	SUM([February]) [February]

		,	CONVERT(decimal(18,0),0) as [March Share Count]
		,	SUM([March]) [March]

		,	CONVERT(decimal(18,0),0) as [April Share Count]
		,	SUM([April]) [April]

		,	CONVERT(decimal(18,0),0) as [May Share Count]
		,	SUM([May]) [May]

		,	CONVERT(decimal(18,0),0) as [June Share Count]
		,	SUM([June]) [June]

		,	CONVERT(decimal(18,0),0) as [July Share Count]
		,	SUM([July]) [July]

		,	CONVERT(decimal(18,0),0) as [August Share Count]
		,	SUM([August]) [August]

		,	CONVERT(decimal(18,0),0) as [September Share Count]
		,	SUM([September]) [September]

		,	CONVERT(decimal(18,0),0) as [October Share Count]
		,	SUM([October]) [October]

		,	CONVERT(decimal(18,0),0) as [November Share Count]
		,	SUM([November]) [November]

		,	CONVERT(decimal(18,0),0) as [December Share Count]
		,	SUM([December]) [December]
	INTO #Revenue
	FROM

	(
		SELECT DISTINCT Account,[January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December]
		FROM #ClientFeeTrns
		PIVOT
		(
		  SUM([Actual Charge])
		 -- SUM([Share Count])
		  FOR Month IN (
			[January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December]
						)
		) AS p
	)  as B
		GROUP BY Account
		Order by Account

	IF OBJECT_ID('Tempdb..#ShareCount') IS NOT NULL DROP TABLE #ShareCount
	SELECT Account

		,	'' as [Agreement Terms]

		,	CONVERT(decimal(18,0),0) as [January Share Count]
		,	SUM([January]) [January]

		,	CONVERT(decimal(18,0),0) as [February Share Count]
		,	SUM([February]) [February]

		,	CONVERT(decimal(18,0),0) as [March Share Count]
		,	SUM([March]) [March]

		,	CONVERT(decimal(18,0),0) as [April Share Count]
		,	SUM([April]) [April]

		,	CONVERT(decimal(18,0),0) as [May Share Count]
		,	SUM([May]) [May]

		,	CONVERT(decimal(18,0),0) as [June Share Count]
		,	SUM([June]) [June]

		,	CONVERT(decimal(18,0),0) as [July Share Count]
		,	SUM([July]) [July]

		,	CONVERT(decimal(18,0),0) as [August Share Count]
		,	SUM([August]) [August]

		,	CONVERT(decimal(18,0),0) as [September Share Count]
		,	SUM([September]) [September]

		,	CONVERT(decimal(18,0),0) as [October Share Count]
		,	SUM([October]) [October]

		,	CONVERT(decimal(18,0),0) as [November Share Count]
		,	SUM([November]) [November]

		,	CONVERT(decimal(18,0),0) as [December Share Count]
		,	SUM([December]) [December]
	INTO #ShareCount
	FROM

	(
		SELECT Account,[January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December]
		FROM #ClientFeeTrns
		PIVOT
		(
		  SUM([Share Count])
		 -- SUM([Share Count])
		  FOR Month IN (
			[January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December]
						)
		) AS p
	)  as B
		GROUP BY Account
		Order by Account

	--Combine
	UPDATE A SET 
		A.[January Share Count] = B.January,
		A.[February Share Count] = B.February,
		A.[March Share Count] = B.March,
		A.[April Share Count] = B.April,
		A.[May Share Count] = B.May,
		A.[June Share Count] = B.June,
		A.[July Share Count] = B.July,
		A.[August Share Count] = B.August,
		A.[September Share Count] = B.September,
		A.[October Share Count] = B.October,
		A.[November Share Count] = B.November,
		A.[December Share Count] = B.December
	FROM #Revenue A Inner Join #ShareCount B
	ON A.Account = B.Account

	--main select query
		SELECT Account, [Agreement Terms] [AgreementTerms]
		,  ISNULL([January Share Count],0) [JanuaryShareCount], ISNULL(January,0) [JanuaryRevenue]
		,  ISNULL([February Share Count],0) [FebruaryShareCount], ISNULL (February,0) [FebruaryRevenue]
		,  ISNULL([March Share Count],0) [MarchShareCount], ISNULL(March,0) [MarchRevenue]
		,  ISNULL([April Share Count],0) [AprilShareCount], ISNULL(April,0) [AprilRevenue]
		,  ISNULL([May Share Count],0) [MayShareCount], ISNULL(May,0) [MayRevenue]
		,  ISNULL([June Share Count],0) [JuneShareCount], ISNULL(June,0) [JuneRevenue]
		,  ISNULL([July Share Count],0) [JulyShareCount], ISNULL(July,0) [JulyRevenue]
		,  ISNULL([August Share Count],0) [AugustShareCount], ISNULL(August,0) [AugustRevenue]
		,  ISNULL([September Share Count],0) [SeptemberShareCount], ISNULL(September,0) [SeptemberRevenue]
		,  ISNULL([October Share Count],0) [OctoberShareCount], ISNULL(October,0) [OctoberRevenue]
		,  ISNULL([November Share Count],0) [NovemberShareCount], ISNULL(November,0) [NovemberRevenue]
		,  ISNULL([December Share Count],0) [DecemberShareCount], ISNULL(December,0) [DecemberRevenue]
		,  ISNULL([January Share Count],0) +ISNULL([February Share Count],0) +ISNULL([March Share Count],0) +ISNULL([April Share Count],0) +ISNULL([May Share Count],0) +ISNULL([June Share Count],0) +ISNULL([July Share Count],0) +ISNULL([August Share Count],0) +ISNULL([September Share Count],0) +ISNULL([October Share Count],0) +ISNULL([November Share Count],0) +ISNULL([December Share Count],0) as [TotalShareCount]
		,  ISNULL([January],0) +ISNULL(February,0) +ISNULL(March,0) +ISNULL(April,0) +ISNULL(May,0) +ISNULL(June,0) +ISNULL(July,0) +ISNULL(August,0) +ISNULL(September,0) +ISNULL(October,0) +ISNULL(November,0) +ISNULL(December,0) [TotalRevenue]
		FROM #Revenue
		where ISNULL(Account,'') != ''
END

--------------------------------------------------
--------------------------------------------------

ELSE IF @Action = 'Get Volumes Charges And Stats'
BEGIN
	DECLARE 
	    @acursor_Office cursor
	   ,@SystemDate SmallDatetime 

	If Object_ID('tempdb..##Stats') IS NOT NULL
		DROP TABLE ##Stats
	Create Table ##Stats
	(
	Record1 varchar(80),
	Record2 varchar(80),
	Record3 varchar(80),
	Record4 varchar(80),
	Record5 varchar(80)
	)

	SET @SystemDate = (SELECT MTX.SystemDate())

	INSERT INTO ##Stats(Record1)
	SELECT 'Volumes and Charges Statistic as of: ' + ETC.FormatDates(@SystemDate,1)

	INSERT INTO ##Stats(Record1)
	SELECT ''

		If @Office = ''
		Begin
			set @acursor_Office = cursor for SELECT Distinct [Roll Up] FROM Fee.MatrixClearingFeeDetails WHERE [Roll Up] != ''  Order by [Roll Up] ASC
			open @acursor_Office
			fetch next from @acursor_Office into @Office
			while (@@fetch_status = 0)
			begin
		
		
			--Set @Office = (SELECT Top 1 Office FROM ETC.ClientAccount WHERE [Company Name] = @Office)

				INSERT INTO ##Stats(Record1)
				SELECT @Office



				INSERT INTO ##Stats(Record1,Record2,Record3,Record4,Record5)
				SELECT 'Month','Year','Share Count','Clearing Fee','Percentage'

				INSERT INTO ##Stats(Record1,Record2,Record3,Record4,Record5)
				SELECT Month, year, 
					ETC.FormatQuantity(SUM([Share Count])),ETC.FormatAccounting(SUM([Actual Charge])), ETC.FormatPercentage(SUM([Percentage]))
				FROM Fee.MatrixClearingFeeDetails
				WHERE [Roll Up] = @Office
				AND Convert(Smalldatetime,Month + ' 01 ' + Year) in (SELECT Distinct Top 6 Convert(Smalldatetime,Month + ' 01 ' + Year) FROM Fee.MatrixClearingFeeDetails ORDER BY Convert(Smalldatetime,Month + ' 01 ' + Year) DESC)
				GROUP BY  [Roll Up],  Month, year
				ORDER BY Convert(Smalldatetime,Month + ' 01 ' + Year) DESC		
		


				INSERT INTO ##Stats(Record1)
				SELECT ''

			 fetch next from @acursor_Office into @Office
			end

			close @acursor_Office
			deallocate @acursor_Office

		End

		----------------------------------------------------------------------------------\
		Else
			Begin

				INSERT INTO ##Stats(Record1)
				SELECT @Office

				Set @Office = (SELECT Top 1 Office FROM ETC.ClientAccount WHERE [Company Name] = @Office)


				INSERT INTO ##Stats(Record1,Record2,Record3,Record4,Record5)
				SELECT 'Month','Year','Share Count','Clearing Fee','Percentage'

				INSERT INTO ##Stats(Record1,Record2,Record3,Record4,Record5)
				SELECT Month, Year, 
						ETC.FormatQuantity(SUM([Share Count])),ETC.FormatAccounting(SUM([Actual Charge])), ETC.FormatPercentage(SUM([Percentage]))
				FROM Fee.MatrixClearingFeeDetails
				WHERE LEFT([Roll Up],3) = @Office
				AND Convert(Smalldatetime,Month + ' 01 ' + Year) in (SELECT Distinct Top 6 Convert(Smalldatetime,Month + ' 01 ' + Year) FROM Fee.MatrixClearingFeeDetails ORDER BY Convert(Smalldatetime,Month + ' 01 ' + Year) DESC)
				GROUP BY  [Roll Up],  Month, year
				ORDER BY Convert(Smalldatetime,Month + ' 01 ' + Year) DESC		
		

			----------------------------------------------------------------------------------
			End

	SELECT *
	FROM ##Stats

	DROP TABLE ##Stats
END --END Get Volumes Charges And Stats

--------------------------------------------------
--------------------------------------------------
ELSE IF @Action = 'Get Calendar'
BEGIN
--------------------------------------------------
	DECLARE 
	@Announcement varchar(1500) = ' SELECT ''Announcement'' [type], CalendarID as [id], Announcement as title, [From Date] as [start], DATEADD(hh,22,[To Date]) as [end], ''#5cb85c'' as color
		  FROM HR.Calendar 
		  where [From Date] >= '''+ convert(varchar(500),@StartDate) +''' and [To Date] <= '''+ convert(varchar(500),@EndDate) +'''
		  AND Type = ''Announcement''',

	@Attendance varchar(1500) = 'SELECT DISTINCT ''Attendance'' [type], 0 as [id], FullName  as title, DateFrom as [start], DATEADD(hh,22,DateTo) as [end], ''#f0ad4e'' as color
		FROM MTX.AbsenceRequest 
		 where DateFrom >='''+ convert(varchar(500),@StartDate) +''' and DateTo <='''+ convert(varchar(500),@EndDate) +'''
		and RequestStatus = ''Approved''',
	@Holiday varchar(1500) = 'UPDATE a set Announcement = ''Company Holiday''
		  FROM HR.Calendar  A
		  WHERE Type = ''Holiday''
		  AND [No Trade] = 1
		  AND Announcement = ''''
       
		  UPDATE a set Announcement = ''NON-Company Holiday''
		  FROM HR.Calendar  A
		  WHERE Type = ''Holiday''
		  AND [No Trade] = 0
		  AND Announcement = '''' 
		    
	SELECT ''Holiday'' [type], CalendarID as [id], [Title]  +''-'' +  Announcement as title, [From Date] as [start], DATEADD(hh,22,[To Date]) [end], ''#f0ad4e'' as color
		  FROM HR.Calendar 
		  where [From Date] >= '''+ convert(varchar(500),@StartDate) +''' and [To Date] <= '''+ convert(varchar(500),@EndDate) +'''
		  AND Type = ''Holiday''',		  

	@RateChange varchar(1500) = 'SELECT ''Rate Change'' [type], 0 as [id], Type + ''='' + (Cast(Amount as Varchar)) as title,[Date From] as [start], null as [end], ''#d9534f'' as color
      FROM ADM.SystemDefaultNumber 
      where [Date From] >= '''+ convert(varchar(500),@StartDate) + '''
      and Status =''Active''',
	 
	@Birthdays varchar(500) = 'SELECT ''Birthday'' [type], 0 as [id], [FirstName] + '' '' + [LastName] as title, dateadd(year, (DATEPART(year,'''+ convert(varchar(500),@StartDate) + ''' ) - year(BirthDay)), BirthDay) as [start],  null as [end], ''#5bc0de'' as color FROM ETC.EmployeeProfile 
	where dateadd(year, (DATEPART(year, '''+ convert(varchar(500),@StartDate) + ''') - year(BirthDay)), BirthDay)  >= '''+ convert(varchar(500),@StartDate) + ''' and dateadd(year, (DATEPART(year,'''+ convert(varchar(500),@StartDate) + ''') - year(BirthDay)), BirthDay) <='''+ convert(varchar(500),@EndDate) + '''
	AND Status = ''Active''',

	@RoomReservation varchar(500) = 'Select ''Room Reservation'' [type],CalendarID as [id], left(convert(varchar, [from date], 108),5) + ''-'' +
            left(convert(varchar, [to date], 108),5) + ''|'' + [Attached file]  + ''-'' +   left(Title,5) as title, 
		   [From Date] as [start],[To Date] [end], ''#5bc0de'' as color
           From hr.Calendar Where Type=''Room Reservation'' and [Send Announcement] = 1 and  convert(date,[From Date]) >= '''+ convert(varchar(500),@StartDate) + ''' and convert(date,[To Date]) <= '''+ convert(varchar(500),@EndDate) + ''''

	if @Type = 'All' 
	BEGIN
			UPDATE a set Announcement = 'Company Holiday'
		  FROM HR.Calendar  A
		  WHERE Type = 'Holiday'
		  AND [No Trade] = 1
		  AND Announcement = ''   
     
		  UPDATE a set Announcement = 'NON-Company Holiday'
		  FROM HR.Calendar  A
		  WHERE Type = 'Holiday'
		  AND [No Trade] = 0
		  AND Announcement = ''  	  

		--SET @SQL = @Holiday + ' UNION ALL ' + @RateChange + ' UNION ALL ' + @Announcement + ' UNION ALL ' + @Attendance + ' UNION ALL ' + @Birthdays + ' UNION ALL ' + @RoomReservation
		SET @SQL = @Holiday + ' UNION ALL ' + @RateChange + ' UNION ALL ' + @Announcement + ' UNION ALL ' + @RoomReservation
	END
	Else if @Type = 'Announcement'
		SET @SQL = @Announcement
	Else if @Type = 'Attendance'
		SET @SQL = @Attendance
	Else if @Type = 'Holiday'
		SET @SQL = @Holiday
	Else if @Type = 'Rate Change'
		SET @SQL = @RateChange
	Else if @Type = 'Birthdays'
		SET @SQL = @Birthdays
	Else if @Type = 'Room Reservation'
		SET @SQL = @RoomReservation
	
	EXEC (@SQL) 
--------------------------------------------------
END

Else if @Action ='AnnouncementAdd'
BEGIN
      INSERT INTO HR.Calendar([Title],[From Date],[To Date],[Announcement], [Send Announcement],[Type],[Attached File], [No Trade], [No Settlement]) 
      Values(@Title,@StartDate,@EndDate,@Note,@Send,@Type,@File,@NoTrade,@NoSettlement) 
END

Else if @Action ='GetCalendarModal'
BEGIN
     SELECT   
           Type, ETC.FormatDates([From Date], 'MM/DD/YYYY') as [datefrom], ETC.FormatDates([To Date], 'MM/DD/YYYY') AS [dateto], Announcement as [note], [Send Announcement] as [sendemail], Title as [title], [Attached File] as [attachedfile], [No Trade] as [notrade], [No Settlement]  as [nosettlement]   
      FROM HR.Calendar
      WHERE CalendarID=@CalendarID
END

Else if @Action ='GetCalendarRoomModal'
BEGIN
     SELECT   
           Type, ETC.FormatDates([From Date], 'MM/DD/YYYY') + ' ' + ETC.FormatDates([From Date], 'H:MMA P') as [datefrom], ETC.FormatDates([To Date], 'MM/DD/YYYY') + ' ' + ETC.FormatDates([To Date], 'H:MMA P') AS [dateto], Announcement as [note], [Send Announcement] as [sendemail], Title as [title], [Attached File] as [attachedfile], [No Trade] as [notrade], [No Settlement]  as [nosettlement]   
      FROM HR.Calendar
      WHERE CalendarID=@CalendarID
END
	
Else if @Action ='AnnouncementEdit'
BEGIN

      UPDATE HR.Calendar Set 
	  [Title]=@Title
	  , [From Date] = @StartDate
	  ,[To Date]= @EndDate
	  , Announcement=@Note
	  ,[Send Announcement]=@Send
      ,[Attached File] = @File 
	  , [No Trade] = @NoTrade
	  , [No Settlement] = @NoSettlement
      where CalendarID =@CalendarID 
END

Else if @Action ='AnnouncementDel'
BEGIN
      DELETE FROM HR.Calendar WHERE [CalendarID] = @CalendarID 
END

Else if @Action ='TestReservation' 
BEGIN
       Select CalendarID from [Matrix].[HR].[Calendar]
       Where
       type = 'Room Reservation' 
       and [Send Announcement] = 1 and [Attached File] = @File
       and (@StartDate between [From Date] and [To Date]
       or @EndDate between [From Date] and [To Date]
       or (@StartDate < [From Date] and @EndDate > [To Date])
       )
END
--------------------------------------------------
--------------------------------------------------

END


EXEC MTX.SPLog_SP @SPName,@Schema,'End'








