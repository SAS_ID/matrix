﻿app.controller('clientRetentionController', ['$scope', 'http', 'dropDownService', 'downloadService', function ($scope, http, dropDownService, downloadService) {
    document.getElementById("btnDownload").style.visibility = "hidden";
    $scope.options = {
        onAnimationComplete: function () {
            document.getElementById("btnDownload").style.visibility = "visible";
        }
    }

    $scope.ddStatuses = [{ value: "Active" }, { value: "New" }, { value: "Closed" }];
    $scope.selectedStatuses = ['New', 'Closed'];
    $scope.unit = {};
    $scope.unit.value = '';
    $scope.series = ['Active', 'New', 'Closed'];

    $scope.chartColors = ['#008000', '#5bc0de', '#DEB887'];
    $scope.tableData = [];
    var _chartData = [];
    $scope.latestData = [];
    $scope.fromMonth = moment().subtract(12, 'months');
    $scope.activePanels = [0];
    $scope.months = dropDownService.getMonths();
    $scope.toYear = { Name: new Date().toJSON().slice(0, 4) };
    $scope.fromYear = { Name: new Date().toJSON().slice(0, 4) - 1 };
    $scope.fromMonth = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth())];
    $scope.toMonth = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth())];

    var view = 'Chart';
    $scope.ChangeView = function () {

        if (view === 'Chart') {
            $scope.activePanels = [0];
            view = 'Table';
        } else {
            $scope.activePanels = [1];
            view = 'Chart';
        }
    }

    $scope.downloadText = 'Download';
    $scope.downloadPDF = function () {
        if ($scope.downloadText === 'Download') {
            $scope.downloadText = 'Downloading..';
            var table = document.getElementById('tableParent').innerHTML;
            var image = document.getElementById("chartImage").toDataURL();
            var legends = document.getElementById("legends").innerHTML;
            var reportParameters = document.getElementById("reportParameters").innerHTML;
            var latestData = document.getElementById("latestData").innerHTML;

            downloadService.pdf(table, image, legends, reportParameters, latestData, 'Client Retention').then(function (res) {
                if (res) {
                    $scope.downloadText = 'Download';
                }
            });
        }
    }

    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    http.post('ClientRetentionUnits', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name) }).success(function (results) {
        $scope.units = JSON.parse($(results).attr('d'));
        $scope.unit = $scope.units[0];
        $scope.ChangeView();
        $scope.ChangeDates();
    });

    $scope.ChangeDates = function () {
        document.getElementById("btnDownload").style.visibility = "hidden";
        //table
        http.post('ClientRetentionTable', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name), Unit: $scope.unit.value }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d')).sort(function (a, b) {
                return new Date(a.Date).getTime() - new Date(b.Date).getTime()
            });
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10 };
        });

        //chart
        $scope.labels = [];
        $scope.data = [[], [], []];

        http.post('GetMonthYearBetween', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name) }).success(function (results) {


            var _dates = JSON.parse($(results).attr('d'));
            for (var dateIndex = 0; dateIndex < _dates.length; dateIndex++) {
                $scope.labels.push(_dates[dateIndex]['Name']);
                $scope.data[0].push(0);
                $scope.data[1].push(0);
                $scope.data[2].push(0);
            }

            var index;
            http.post('ClientRetentionChart', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name), Unit: $scope.unit.value }).success(function (results) {
                _chartData = JSON.parse($(results).attr('d'));

                for (var i = 0; i < _chartData.length; i++) {
                    index = $scope.labels.indexOf(_chartData[i].Date);

                    if (index !== -1) {
                        if (_chartData[i].Status === 'Active') {
                            $scope.data[0][index] = _chartData[i]['Count'];
                        } else if (_chartData[i].Status === 'New') {
                            $scope.data[1][index] = _chartData[i]['Count'];
                        } else if (_chartData[i].Status === 'Closed') {
                            $scope.data[2][index] = _chartData[i]['Count'];
                        }
                    }
                }
                // to get the latest data for report
                $scope.latestData = [];

                for (var i = 0; i < $scope.series.length; i++) {
                    $scope.latestData[i] = { value: $scope.data[i][$scope.data[i].length - 1], field: $scope.series[i], color: $scope.chartColors[i] };
                }

            });
        });

    }

    //sorting
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };
    $scope.predicate = '';
    $scope.reverse = false;
    $scope.customFilter = function (item) {
        if (item.Status === $scope.selectedStatuses[0] || item.Status === $scope.selectedStatuses[1] || item.Status === $scope.selectedStatuses[2]) {
            return item;
        }
    };

}]);

