﻿app.controller('loginController', ['$scope', 'authService', '$http', 'appSettings', 'localStorageService', '$location',
    function ($scope, authService, $http, appSettings, localStorageService, $location) {
        var getParameterByName = function (name) {
            var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.hash);
            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }

        var firstName = getParameterByName('firstname');
        var lastName = getParameterByName('lastname');
        var fullName = firstName + ' ' + lastName;


        $http.post(appSettings.serviceBase + 'LogInByFullName', { FullName: fullName }).success(function (results) {

            var user = JSON.parse($(results).attr('d'))[0];
            if (user) {
                //login user
                localStorageService.set('authClientWeb', {
                    token: user.EmployeeID,
                    firstName: user.FirstName,
                    fullName: user.FullName,
                    isAuth: true,
                    isManager: user.isManager
                });
                authService.fillAuth();
                $scope.user = user.FirstName;
                $scope.isManager = user.isManager;
                $location.path('/dashboard?');
            }
            else {
                window.location.hash = "#/blank";
            }
        }).error(function (err, status) {
            alert("Your login attempt was not successful.");
        });

    }]);