﻿'use strict';
app.controller('yearlyRevenueAndSaleController', ['$scope', 'http', 'dropDownService', function ($scope, http, dropDownService) {
    $scope.columns = [{ name: "Account", ticked: true },
            { name: "Agreement Terms", ticked: true },
            { name: "Jan Revenue", ticked: true },
            { name: "Jan Share Count", ticked: true },
            { name: "Feb Revenue", ticked: true },
            { name: "Feb Share Count", ticked: true },
            { name: "Mar Revenue", ticked: true },
            { name: "Mar Share Count", ticked: true },
            { name: "Apr Revenue", ticked: true },
            { name: "Apr Share Count", ticked: true },
            { name: "May Revenue", ticked: true },
            { name: "May Share Count", ticked: true },
            { name: "Jun Revenue", ticked: true },
            { name: "Jun Share Count", ticked: true },
            { name: "Jul Revenue", ticked: true },
            { name: "Jul Share Count", ticked: true },
            { name: "Aug Revenue", ticked: true },
            { name: "Aug Share Count", ticked: true },
            { name: "Sep Revenue", ticked: true },
            { name: "Sep Share Count", ticked: true },
            { name: "Oct Revenue", ticked: true },
            { name: "Oct Share Count", ticked: true },
            { name: "Nov Revenue", ticked: true },
            { name: "Nov Share Count", ticked: true },
            { name: "Dec Revenue", ticked: true },
            { name: "Dec Share Count", ticked: true },
            { name: "Total Share Count", ticked: true },
            { name: "Total Revenue", ticked: true }
    ];

    $scope.year = { Name: new Date().toJSON().slice(0, 4) };
    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    $scope.tableData = [];
        
    $scope.LoadTable = function () {
        http.post('GetYearlyRevenueAndSale', { Year: $scope.year.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10};
        });


        $scope.CalculateTotal = function (column) {
            var total = 0;
            for (var i = 0; i < $scope.tableData.length; i++) {
                var data = $scope.tableData[i][column];
                total += data;
            } 
            return +(total).toFixed(2);;
        }
    }
    $scope.LoadTable();



    //sorting
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.predicate = 'Account';
    $scope.reverse = false;
}]);


