﻿'use strict';
app.controller('salesCommissionController', ['$scope', 'authService', 'http', 'dropDownService', 'downloadService', function ($scope, authService, http, dropDownService, downloadService) {

    $scope.year = { Name: new Date().toJSON().slice(0, 4) };
    $scope.months = dropDownService.getMonths();
    $scope.month = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth() - 1)];
    $scope.salesRep = { Name: '' };
    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });
    http.post('GetSalesRep', '').success(function (results) {
        $scope.salesReps = JSON.parse($(results).attr('d'));
    });
    
    $scope.tableData = [];
    $scope.tableData2 = [];

    $scope.LoadTable = function () {
        http.post('GetSalesCommissionSummary', { Month: $scope.month.Name, Year: $scope.year.Name }).success(function (results) {  
            $scope.tableData = JSON.parse($(results).attr('d'));
        });

        http.post('GetSalesCommissionDetails', { Month: $scope.month.Name, Year: $scope.year.Name }).success(function (results) {
            $scope.tableData2 = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData2.length, size: 10 };
        });
    }
    $scope.LoadTable();

    $scope.RefreshReport = function () {
        http.post('SalesCommissionRefreshReport', { Month: $scope.month.Name, Year: $scope.year.Name }).success(function (results) {});
    }

    $scope.downloadText = 'Download';
    $scope.downloadPDF = function () {
        if ($scope.downloadText === 'Download') {
            $scope.downloadText = 'Downloading..';
            var table = document.getElementById('tableParent').innerHTML;
            var image = '';
            var legends = '';
            var reportParameters = document.getElementById('reportParameters').innerHTML;;
            var latestData = '';
            var summary = document.getElementById('summaryTable').innerHTML;
            table = summary + table;
            downloadService.pdf(table, image, legends, reportParameters, latestData, 'Sales Commission').then(function (res) {
                if (res) {
                    $scope.downloadText = 'Download';
                }
            });
        }
    };

    //sorting
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.predicate = 'Account';
    $scope.reverse = false;
}]);


