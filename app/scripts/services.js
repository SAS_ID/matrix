﻿
'use strict';
app.factory('authService', ['localStorageService', '$http', 'appSettings', function (localStorageService, $http, appSettings) {

    var authService = {};

    var data = {
        isAuth: false,
        firstName: "",
        fullName: "",
        token: "",
        isManager: false
    }

    authService.logOut = function () {
        localStorageService.remove('authClientWeb');
        data.isAuth = false,
        data.firstName = "",
        data.fullName = "",
        data.token = "",
        data.isManager = false
    }

    var _fillAuth = function () {
        var auth = localStorageService.get('authClientWeb');
        if (auth) {
            data.isAuth = true;
            data.firstName = auth.firstName;
            data.token = auth.token;
            data.fullName = auth.fullName;
            data.isManager = auth.isManager;
        } else {
            authService.logOut();
        }
    }

    authService.fillAuth = _fillAuth;
    authService.authUser = data;

    return authService;
}]);


app.factory('valuesRedirectService', [function () {
    var values = [];

    return {
        post: function (scopes) {
            values = scopes;
        },
        get: function () {
            return values;
        }
    }
}]);

app.factory('http', ['$http', 'appSettings', function ($http, appSettings) {
    return {
        post: function (path, param) {
            return $http.post(appSettings.serviceBase + path, param)
        },
        get: function (path) {
            return $http.get(path);
        }
    }
}]);
app.factory('downloadService', ['$http', 'appSettings', '$q', function ($http, appSettings, $q) {
    
    return {
        
        pdf: function (table, image, legends, reportParameters, latestData, filename, otherOptions) {
            var deferred = $q.defer();
            var report = '';

            function readTextFile(file) {
                var client = new XMLHttpRequest();
                client.open('GET', file);
                client.onloadend = function () {
                    report = client.responseText;
                    savePDF();
                }
                client.send();
            }
            var template = 'app/reportTemplate.html';
            if (otherOptions && otherOptions.template) {
                template = otherOptions.template;
            }
            if (console) {
                console.log('Using ' + template + ' to export as PDF.');
            }
            readTextFile(template);

            //function unicodeStringToTypedArray(s) {
            //    var escstr = encodeURIComponent(s);
            //    var binstr = escstr.replace(/%([0-9A-F]{2})/g, function (match, p1) {
            //        return String.fromCharCode('0x' + p1);
            //    });
            //    var ua = new Uint8Array(binstr.length);
            //    Array.prototype.forEach.call(binstr, function (ch, i) {
            //        ua[i] = ch.charCodeAt(0);
            //    });
            //    return ua;
            //}

            function savePDF() {
                report = report.replace(/title_here/g, filename);
                report = report.replace('reportParameters_here', reportParameters);
                report = report.replace('table_here', table);
                report = report.replace('legends_here', legends);
                report = report.replace('image_here', image);
                report = report.replace('latestData_here', latestData);

                var converterUrl = appSettings.converterBase + '/html2pdf';
                //var converterUrl = 'http://localhost:8080/converter-api/converters/html2pdf';
                if (otherOptions) {
                    if (otherOptions.orientation) {
                        converterUrl += ('/' + otherOptions.orientation);
                    }
                    if (otherOptions.paperSize) {
                        converterUrl += ('/' + otherOptions.paperSize);
                    }
                }
                if (console) {
                    console.log('Using ' + converterUrl + ' to export as PDF.');
                }
                $http({
                    method: 'POST',
                    url: converterUrl,
                    dataType: 'html',
                    data: report,
                    headers: {
                        'Content-Type': 'text/html'
                    },
                    responseType: 'arraybuffer'
                }).success(function (data) {
                    var file = new Blob([data], { type: 'application/pdf' });
                    saveAs(file, filename + '.pdf');
                    deferred.resolve(true);

                });
                
                //var file = new Blob([unicodeStringToTypedArray(report)], { type: 'application/pdf' });
                //saveAs(file, filename + '.html');
            }
            return deferred.promise;
        }
    }
}]);

app.factory('dropDownService', [function () {
    return {
        getMonths: function () {
            return [{ Name: 'January' }, { Name: 'February' }, { Name: 'March' }, { Name: 'April' }, { Name: 'May' }, { Name: 'June' }, { Name: 'July' }, { Name: 'August' }, { Name: 'September' }, { Name: 'October' }, { Name: 'November' }, { Name: 'December' }];
        },
        getUnits: function () {
            return [{ Name: 'ETCGG - ETC Global Group, LLC' }, { Name: 'ETC - Electronic Transaction Clearing, Inc' }, { Name: 'ETCC - ETC Canada' }];
        }
    }
}]);


