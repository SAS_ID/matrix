﻿'use strict';
app.controller('monthlyClearingValidationController', ['$scope', 'authService', 'http', 'dropDownService', function ($scope, authService, http, dropDownService) {
    $scope.columns = [{ name: "Company Name", ticked: true },
            { name: "Roll Up", ticked: true },
            { name: "Share Count", ticked: true },
            { name: "Rate", ticked: true },
            { name: "Charge Per Share", ticked: true },
            { name: "Actual Charge", ticked: true },
            { name: "Fee Schedule", ticked: true },
            { name: "Fee Matrix", ticked: true },
            { name: "Account", ticked: true }
    ];

    $scope.year = { Name: new Date().toJSON().slice(0, 4) };
    $scope.months = dropDownService.getMonths();
    $scope.month = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth() - 1)];
    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    $scope.tableData = [];

    $scope.LoadTable = function () {
        http.post('GetMonthlyClearingValidation', { Month: $scope.month.Name, Year: $scope.year.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10 };
        });

        $scope.CalculateTotal = function (column) {
            var total = 0;
            for (var i = 0; i < $scope.tableData.length; i++) {
                var data = $scope.tableData[i][column];
                total += data;
            }
            return total;
            //return +(total).toFixed(2);
        }
    }
    $scope.LoadTable();

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.predicate = 'Name';
    $scope.reverse = false;

}]);