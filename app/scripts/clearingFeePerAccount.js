﻿'use strict';
app.controller('clearingFeePerAccountController', ['$scope', 'http', 'dropDownService', function ($scope, http, dropDownService) {
    $scope.columns = [{ name: "Correspondent" },
            { name: "Office"},
            { name: "Account No"},
            { name: "Company Name"},
            { name: "Roll Up"},
            { name: "Share Count" },
            { name: "Rate"},
            { name: "Clearing Fee" }
    ];


    $scope.rollUp = { Name: '' };
    $scope.year = { Name: new Date().toJSON().slice(0, 4) };
    $scope.months = dropDownService.getMonths();
    $scope.month = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth() - 1)];
    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    $scope.LoadTable = function () {
        http.post('GetClearingFeePerAccount', { Month: $scope.month.Name, Year: $scope.year.Name, RollUp: $scope.rollUp.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10 };
        });
    }

    $scope.GetRollUps = function () {
        http.post('GetRollUp', { Month: $scope.month.Name, Year: $scope.year.Name }).success(function (results) {
            $scope.rollUp = { Name: '' };
            $scope.rollUps = JSON.parse($(results).attr('d'));
            $scope.LoadTable();
        });

    }

    $scope.GetRollUps();

    $scope.tableData = [];


    

    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.predicate = 'RollUp';
    $scope.reverse = false;
}]);


