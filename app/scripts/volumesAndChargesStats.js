﻿'use strict';
app.controller('volumesAndChargesStatsController', ['$scope', 'http', function ($scope, http) {

    $scope.company = { Name: '' };

    http.post('GetCompanies', '').success(function (results) {
        $scope.companies = JSON.parse($(results).attr('d'));
    });


    $scope.tableData = [];


    $scope.LoadTable = function () {
        http.post('GetVolumesChargesAndStats', { Company: $scope.company.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
        });
    }
    $scope.LoadTable();
    //set css for headers titles
    $scope.isIncludedInList = function (index) {
        if (index === 0) {
            return true;
        }
        return false;
    }
    //set css for headers titles
    $scope.isIncludedInList2 = function (value, value2, index) {
        if (value && !value2 && index != 0) {
            return true;
        }
        return false;
    }
    //set css total bold style
    $scope.isIncludedInList3 = function (value) {
        if (value === "Month") {
            return true;
        }
        return false;
    }


}]);


