﻿'use strict';
app.controller('calendarController', ['$scope', 'http', '$modal', function ($scope, http, $modal) {

    $scope.selectedEvent = '';
    $scope.eventType = { Name: 'All' };
    $scope.eventTypes = [{ Name: 'All' }, { Name: 'Announcement' },{ Name: 'Holiday' }, { Name: 'Rate Change' }, { Name: 'Room Reservation' }];
    $scope.viewRoom = 'CF1';
    $scope.viewRooms = ['CF1', 'CF2', 'CF3', 'CF4'];


    //var modal = $modal({ scope: $scope, templateUrl: 'app/views/modalCalendar.html', show: false, backdrop: 'static', controller: 'modalCalendarController' });
    //$scope.showModal = function (id, header) {
    //    $scope.calendarID = id;
    //    $scope.header = header;
    //    $scope.CloseModal = function () {
    //        modal.hide();
    //    }
    //    modal.$promise.then(modal.show);
    //};

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next,today',
            center: 'title',
            right: 'today,prev,next'
        },
       
        businessHours: true, // display business hours
        eventLimit: true,
        displayEventTime: false,
        events: function (start, end, timezone, callback) {
            http.post('GetCalendar', { StartDate: start._d, EndDate: end._d, Type: $scope.eventType.Name }).success(function (results) {

                callback(JSON.parse($(results).attr('d')));
            });
        },
        eventRender: function (event, element) {
            element.tooltipster({
                content: $('<div class="calendartooltip"><table><tr><td class="calendardesc">' + (event.type || '') + ':</td><td>' + (event.title || '') + '</td></tr></table></div>'),
                delay: 500,
                trigger: 'hover',
                theme: 'tooltipster-default',
                animation: 'fade',
                contentAsHTML: true,
                contentCloning: false
            });
        },

        eventClick: function (calEvent, jsEvent, view) {
            $scope.showModal(calEvent);
        }

    });

    $scope.ReloadCalendar = function () {
        $('#calendar').fullCalendar('refetchEvents')
    }


    var modal = $modal({ scope: $scope, templateUrl: 'app/views/modalCalendar.html', show: false, backdrop: 'static', controller: 'modalCalendarController' });
    $scope.showModal = function (selectedEvent) {
        $scope.calendarID = selectedEvent.id;
        $scope.header = selectedEvent.type + ' : ' + selectedEvent.title;
        $scope.selectedEvent = selectedEvent;
        $scope.CloseModal = function () {
            modal.hide();
        }
        modal.$promise.then(modal.show);
    };

}]);





