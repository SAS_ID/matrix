﻿'use strict';
app.controller('modalCalendarController', ['$scope', 'authService', 'http', '$alert', '$timepicker', 'dropDownService', function ($scope, authService, http, $alert, $timepicker, dropDownService) {
    $scope.calendar = [];

    if ($scope.selectedEvent.type === 'Announcement') {
        $scope.activePage = 'app/views/types/announcement.html';
    } else if ($scope.selectedEvent.type === 'Holiday') {
        $scope.activePage = 'app/views/types/holiday.html';
    } else if ($scope.selectedEvent.type === 'Room Reservation') {
        $scope.activePage = 'app/views/types/roomreservation.html';
    } else if ($scope.eventType.Name === 'Announcement') {
        $scope.header = 'New Entry';
        $scope.activePage = 'app/views/types/announcement.html';
    } else if ($scope.eventType.Name === 'Holiday') {
        $scope.header = 'New Entry';
        $scope.activePage = 'app/views/types/holiday.html';
    } else if ($scope.eventType.Name === 'Room Reservation') {
        $scope.header = 'New Entry';
        $scope.calendarID = 0;
        //$scope.calendar.attachedfile = '';
        $scope.activePage = 'app/views/types/roomreservation.html';
    } else if ($scope.selectedEvent.type === 'Attendance') {
        $scope.activePage = '';
        $scope.selectedEvent.start = dropDownService.getMonths()[new Date($scope.selectedEvent.start).getMonth()].Name + ' ' + new Date($scope.selectedEvent.start).getDate() + ', ' + new Date($scope.selectedEvent.start).getFullYear();
        $scope.selectedEvent.end = dropDownService.getMonths()[new Date($scope.selectedEvent.end).getMonth()].Name + ' ' + new Date($scope.selectedEvent.end).getDate() + ', ' + new Date($scope.selectedEvent.end).getFullYear();
    } else if ($scope.selectedEvent.type === 'Birthday') {
        $scope.activePage = '';
        $scope.selectedEvent.start = dropDownService.getMonths()[new Date($scope.selectedEvent.start).getMonth()].Name + ' ' + new Date($scope.selectedEvent.start).getDate() + ', ' + new Date($scope.selectedEvent.start).getFullYear();
        $scope.selectedEvent.end = dropDownService.getMonths()[new Date($scope.selectedEvent.end).getMonth()].Name + ' ' + new Date($scope.selectedEvent.end).getDate() + ', ' + new Date($scope.selectedEvent.end).getFullYear();
    } else if ($scope.selectedEvent.type === 'Rate Change') {
        $scope.activePage = '';
        $scope.selectedEvent.start = dropDownService.getMonths()[new Date($scope.selectedEvent.start).getMonth()].Name + ' ' + new Date($scope.selectedEvent.start).getDate() + ', ' + new Date($scope.selectedEvent.start).getFullYear();
        $scope.selectedEvent.end = dropDownService.getMonths()[new Date($scope.selectedEvent.end).getMonth()].Name + ' ' + new Date($scope.selectedEvent.end).getDate() + ', ' + new Date($scope.selectedEvent.end).getFullYear();
    }



    //else {
    //    $scope.activePage = '';

    //    $scope.selectedEvent.start = dropDownService.getMonths()[new Date($scope.selectedEvent.start).getMonth()].Name + ' ' + new Date($scope.selectedEvent.start).getDate() + ', ' + new Date($scope.selectedEvent.start).getFullYear();
    //    $scope.selectedEvent.end = dropDownService.getMonths()[new Date($scope.selectedEvent.end).getMonth()].Name + ' ' + new Date($scope.selectedEvent.end).getDate() + ', ' + new Date($scope.selectedEvent.end).getFullYear();

    //}

    var _fileReader = new FileReader();
    $scope.saveAnnounce = function () {
        var file = document.getElementById('file').files[0];
        var message = '';
        if (!$scope.calendar.datefrom) {
            message += '<br />Date From is required.';
        }
        if (!$scope.calendar.dateto) {
            message += '<br />Date To is required.';
        }
        if (!$scope.calendar.title) {
            message += '<br />Title is required.';
        }
        if ($scope.calendar.datefrom > $scope.calendar.dateto) {
            message += '<br />[Date From] must be less than [Date to] and [Start Date] must be less than [End Date]';
        }
        if (!$scope.calendar.note) {
            message += '<br />Note is required.';
        }
        if (!message) {
            if (!file) {
                http.post('AddOrEditCalendarAnnouncementNoUpload', {
                    CalendarID: $scope.calendarID ? $scope.calendarID : 0,
                    Type: $scope.eventType.Name ? $scope.eventType.Name : '',
                    FromDate: $scope.calendar.datefrom ? new Date($scope.calendar.datefrom) : new Date('01-01-1900'),
                    ToDate: $scope.calendar.dateto ? new Date($scope.calendar.dateto) : new Date('01-01-1900'),
                    Title: $scope.calendar.title ? $scope.calendar.title : '',
                    Announcement: $scope.calendar.note ? $scope.calendar.note : '',
                    AttachedFile: $scope.calendar.attachedfile ? $scope.calendar.attachedfile : '',
                    SendAnnouncement: $scope.calendar.sendemail ? $scope.calendar.sendemail : false,
                    File: btoa(_fileReader.result),
                    //AttachedFile: document.getElementById('file').value.split(/(\\|\/)/g).pop(),
                    //ModifiedBy: authService.authUser.token,
                }).success(function (results) {
                    $scope.CloseModal();
                    $scope.ReloadCalendar();
                    $alert({
                        title: 'Announcement has been saved',
                        content: '&nbsp',
                        placement: 'top-rightModalAlert',
                        type: 'success',
                        show: true,
                        duration: 3
                    });

                });

            }
            else {
                _fileReader.onload = function () {
                    http.post('AddOrEditCalendarAnnouncement', {
                        CalendarID: $scope.calendarID ? $scope.calendarID : 0,
                        Type: $scope.eventType.Name ? $scope.eventType.Name : '',
                        FromDate: $scope.calendar.datefrom ? new Date($scope.calendar.datefrom) : new Date('01-01-1900'),
                        ToDate: $scope.calendar.dateto ? new Date($scope.calendar.dateto) : new Date('01-01-1900'),
                        Title: $scope.calendar.title ? $scope.calendar.title : '',
                        Announcement: $scope.calendar.note ? $scope.calendar.note : '',
                        //AttachedFile: $scope.calendar.attachedfile ? $scope.calendar.attachedfile : '',
                        SendAnnouncement: $scope.calendar.sendemail ? $scope.calendar.sendemail : false,
                        File: btoa(_fileReader.result),
                        AttachedFile: document.getElementById('file').value.split(/(\\|\/)/g).pop()
                        //ModifiedBy: authService.authUser.token,
                    }).success(function (results) {
                        $scope.CloseModal();
                        $scope.ReloadCalendar();
                        $alert({
                            title: 'Announcement has been saved',
                            content: '&nbsp',
                            placement: 'top-rightModalAlert',
                            type: 'success',
                            show: true,
                            duration: 3
                        });

                    });
                }

                _fileReader.readAsBinaryString(file);
            }
        }
        else {
            $alert({
                title: 'Invalid Request',
                content: message + '&nbsp&nbsp',
                placement: 'top-rightModalAlert',
                type: 'danger',
                show: true,
                duration: 3
            });
        }
    }



    //add or edit view Calendar Holiday
    //if ($scope.calendarID) {
    //    http.post('GetCalendarModal', { CalendarID: $scope.calendarID }).success(function (results) {
    //        $scope.calendar = JSON.parse($(results).attr('d'))[0];
    //    });
    //}

    var _fileReader = new FileReader();
    $scope.saveHoliday = function () {
        var file = document.getElementById('file').files[0];
        var message = '';
        if (!$scope.calendar.datefrom) {
            message += '<br />Date From is required.';
        }
        if (!$scope.calendar.dateto) {
            message += '<br />Date To is required.';
        }
        if (!$scope.calendar.title) {
            message += '<br />Title is required.';
        }
        if ($scope.calendar.datefrom > $scope.calendar.dateto) {
            message += '<br />[Date From] must be less than [Date to] and [Start Date] must be less than [End Date]';
        }
        if (!$scope.calendar.note) {
            message += '<br />Note is required.';
        }
        if (!message) {
            if (!file) {

                http.post('AddOrEditCalendarHolidayNoUpload', {
                    CalendarID: $scope.calendarID ? $scope.calendarID : 0,
                    Type: $scope.eventType.Name ? $scope.eventType.Name : '',
                    FromDate: $scope.calendar.datefrom ? new Date($scope.calendar.datefrom) : new Date('01-01-1900'),
                    ToDate: $scope.calendar.dateto ? new Date($scope.calendar.dateto) : new Date('01-01-1900'),
                    Title: $scope.calendar.title ? $scope.calendar.title : '',
                    Announcement: $scope.calendar.note ? $scope.calendar.note : '',
                    AttachedFile: $scope.calendar.attachedfile ? $scope.calendar.attachedfile : '',
                    SendAnnouncement: $scope.calendar.sendemail ? $scope.calendar.sendemail : false,
                    NoTrade: $scope.calendar.notrade ? $scope.calendar.notrade : false,
                    NoSettlement: $scope.calendar.nosettlement ? $scope.calendar.nosettlement : false,
                    File: btoa(_fileReader.result),
                    //AttachedFile: document.getElementById('file').value.split(/(\\|\/)/g).pop()
                    //ModifiedBy: authService.authUser.token,
                }).success(function (results) {
                    $scope.CloseModal();
                    $scope.ReloadCalendar();
                    $alert({
                        title: 'Holiday has been saved',
                        content: '&nbsp',
                        placement: 'top-rightModalAlert',
                        type: 'success',
                        show: true,
                        duration: 3
                    });
                });
            }
            else {
                _fileReader.onload = function () {
                    http.post('AddOrEditCalendarHoliday', {
                        CalendarID: $scope.calendarID ? $scope.calendarID : 0,
                        Type: $scope.eventType.Name ? $scope.eventType.Name : '',
                        FromDate: $scope.calendar.datefrom ? new Date($scope.calendar.datefrom) : new Date('01-01-1900'),
                        ToDate: $scope.calendar.dateto ? new Date($scope.calendar.dateto) : new Date('01-01-1900'),
                        Title: $scope.calendar.title ? $scope.calendar.title : '',
                        Announcement: $scope.calendar.note ? $scope.calendar.note : '',
                        //AttachedFile: $scope.calendar.attachedfile ? $scope.calendar.attachedfile : '',
                        SendAnnouncement: $scope.calendar.sendemail ? $scope.calendar.sendemail : false,
                        NoTrade: $scope.calendar.notrade ? $scope.calendar.notrade : false,
                        NoSettlement: $scope.calendar.nosettlement ? $scope.calendar.nosettlement : false,
                        File: btoa(_fileReader.result),
                        AttachedFile: document.getElementById('file').value.split(/(\\|\/)/g).pop()
                        //ModifiedBy: authService.authUser.token,
                    }).success(function (results) {
                        $scope.CloseModal();
                        $scope.ReloadCalendar();
                        $alert({
                            title: 'Holiday has been saved',
                            content: '&nbsp',
                            placement: 'top-rightModalAlert',
                            type: 'success',
                            show: true,
                            duration: 3
                        });
                    });
                }
                _fileReader.readAsBinaryString(file);
            }
        }
        else {
            $alert({
                title: 'Invalid Request',
                content: message + '&nbsp&nbsp',
                placement: 'top-rightModalAlert',
                type: 'danger',
                show: true,
                duration: 3
            });
        }
    }


    //add or edit view Calendar Room Reservation
    if ($scope.calendarID) {
        http.post('GetCalendarRoomModal', { CalendarID: $scope.calendarID }).success(function (results) {
            $scope.calendar = JSON.parse($(results).attr('d'))[0];
        });

    }

    $scope.saveRoomReserve = function () {

        var message = '';

        if (!$scope.calendar.datefrom) {
            message += '<br />Date From is required.';
        }
        if (!$scope.calendar.dateto) {
            message += '<br />Date To is required.';
        }
        if ($scope.calendar.datefrom >= $scope.calendar.dateto) {
            message += '<br />[Date From] must be less than [Date to] and [Start Date] must be less than [End Date]';
        }
        if (!$scope.calendar.note) {
            message += '<br />Note is required.';
        }
        if (!$scope.calendar.attachedfile) {
            message += '<br />Room is required.';
        }


        if (!message) {
            http.post('TestReservation', { CalendarID: $scope.calendarID ? $scope.calendarID : 0, FromDate: new Date($scope.calendar.datefrom), ToDate: new Date($scope.calendar.dateto), AttachedFile: $scope.calendar.attachedfile }).success(function (results) {
                var conflictSchedID;


                if (JSON.parse($(results).attr('d'))[0]) {
                    //conflictSchedID = JSON.parse($(results).attr('d'))[0].CalendarID;
                    conflictSchedID = JSON.parse($(results).attr('d'))[0].CalendarID;
                } else {
                    conflictSchedID = 0;
                }

                if (conflictSchedID != $scope.calendarID) {
                    message += '<br />Schedule in conflict, please choose a different Room/Schedule';
                    $alert({
                        title: 'Invalid Request',
                        content: message + '&nbsp&nbsp',
                        placement: 'top-rightModalAlert',
                        type: 'danger',
                        show: true,
                        duration: 3
                    });

                } else {

                    http.post('AddOrEditCalendarRoomReserve', {
                        CalendarID: $scope.calendarID ? $scope.calendarID : 0,
                        Title: authService.authUser.fullName,
                        Type: $scope.eventType.Name,
                        FromDate: new Date($scope.calendar.datefrom),
                        ToDate: new Date($scope.calendar.dateto),
                        Announcement: $scope.calendar.note,
                        AttachedFile: $scope.calendar.attachedfile,
                    }).success(function (results) {
                        $scope.CloseModal();
                        $scope.ReloadCalendar();
                        $alert({
                            title: 'Room Reservation has been saved',
                            content: '&nbsp',
                            placement: 'top-rightModalAlert',
                            type: 'success',
                            show: true,
                            duration: 3
                        });
                    });

                    //http.post('AddOrEditCalendarRoomReserve', {
                    //    CalendarID: $scope.calendarID ? $scope.calendarID : 0,
                    //    Title: authService.authUser.fullName,
                    //    Type: $scope.eventType.Name ? $scope.eventType.Name : '',
                    //    FromDate: $scope.calendar.datefrom ? new Date($scope.calendar.datefrom) : new Date('01-01-1900'),
                    //    ToDate: $scope.calendar.dateto ? new Date($scope.calendar.dateto) : new Date('01-01-1900'),
                    //    Announcement: $scope.calendar.note ? $scope.calendar.note : '',
                    //    AttachedFile: $scope.calendar.attachedfile ? $scope.calendar.attachedfile : '',
                    //}).success(function (results) {
                    //    $scope.CloseModal();
                    //    $scope.ReloadCalendar();
                    //    $alert({
                    //        title: 'Room Reservation has been saved',
                    //        content: '&nbsp',
                    //        placement: 'top-rightModalAlert',
                    //        type: 'success',
                    //        show: true,
                    //        duration: 3
                    //    });
                    //});
                }



            });
        } else {
            $alert({
                title: 'Invalid Request',
                content: message + '&nbsp&nbsp',
                placement: 'top-rightModalAlert',
                type: 'danger',
                show: true,
                duration: 3
            });
        }
    }

    $scope.delete = function (calendarID) {
        http.post('DeleteCalendar', { CalendarID: $scope.calendarID }).success(function (results) {
            $scope.CloseModal();
            $scope.ReloadCalendar();
            $alert({
                title: 'Event was deleted',
                content: '&nbsp',
                placement: 'top-rightModalAlert',
                type: 'success',
                show: true,
                duration: 3
            });

        });
    }

    //file uploader
    $scope.upload = function () {
        var file = document.getElementById('file').files[0];
        if (!file) {
            $alert({
                title: 'No file',
                content: ' selected.&nbsp&nbsp',
                placement: 'top-rightModalAlert',
                type: 'danger',
                show: true,
                duration: 3
            });
            return;

        }

        else {
            $scope.calendar.attachedfile = document.getElementById('file').value;
            $alert({
                title: 'Uploaded',
                content: ' successfully.&nbsp&nbsp',
                placement: 'top-rightModalAlert',
                type: 'success',
                show: true,
                duration: 3
            });
        }
    }
    $scope.fileNameChanged = function () {
        $scope.upload();
    }

    $scope.IntroOptionsCalendar = {
        steps: [
       {
           element: "#one",
           intro: "Complete this form to add/edit an calendar details.",
           position: 'right'
       }

        ],
        showStepNumbers: false,
        disableInteraction: true,
        exitOnOverlayClick: true,
        exitOnEsc: true,
        scrollToElement: true


    };

}]);





