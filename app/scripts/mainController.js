﻿'use strict';
app.controller('mainController', ['$scope', 'authService', '$http', 'appSettings', 'localStorageService', '$location', '$anchorScroll', '$rootScope',
    function ($scope, authService, $http, appSettings, localStorageService, $location, $anchorScroll, $rootScope) {
        //===================================== SET YEAR IN FOOTER ===========================

        $scope.year = new Date().toJSON().slice(0, 4);

        //===================================== ACCESS CONTROL ===========================
        var AccessControl = function () {

            $scope.isLinkVisible = {};

            if (!authService.authUser.isAuth) {
                return;
            }

            if (authService.authUser.isManager) {
                $scope.isLinkVisible.dashBoard = true;
            }

            $http.post(appSettings.serviceBase + 'UserPagesAccess', { EmployeeID: authService.authUser.token }).success(function (results) {
                var links = JSON.parse($(results).attr('d'));

                for (var i = 0; i < links.length; i++) {

                    if (links[i].Name === 'Board Statistics') {
                        $scope.isLinkVisible.boardStatistics = true;
                    } else if (links[i].Name === 'Calendar') {
                        $scope.isLinkVisible.calendar = true;
                    } else if (links[i].Name === 'Monthly Clearing') {
                        $scope.isLinkVisible.monthlyClearing = true;
                    } else if (links[i].Name === 'Employee Profile') {
                        $scope.isLinkVisible.employeeProfile = true;
                    } else if (links[i].Name === 'Volumes And Charges Stats') {
                        $scope.isLinkVisible.volumesAndChargesStats = true;
                    } else if (links[i].Name === 'Company Directory') {
                        $scope.isLinkVisible.companyDirectory = true;
                    } else if (links[i].Name === 'Monthly Clearing Validation') {
                        $scope.isLinkVisible.monthlyClearingValidation = true;
                    } else if (links[i].Name === 'Sales Commission') {
                        $scope.isLinkVisible.salesCommission = true;
                    } else if (links[i].Name === 'Clearing Fee Per Account') {
                        $scope.isLinkVisible.clearingFeePerAccount = true;
                    } else if (links[i].Name === 'Yearly Revenue and Share') {
                        $scope.isLinkVisible.yearlyRevenueAndShare = true;
                    } else if (links[i].Name === 'Employee Portal') {
                        $scope.isLinkVisible.employeePortal = true;
                    } else if (links[i].Name === 'Client Retention') {
                        $scope.isLinkVisible.clientRetention = true;
                    } else if (links[i].Name === 'Actual vs. Proj. Pipeline Rev.') {
                        $scope.isLinkVisible.actualVsProjPipelineRev = true;
                    }
                }
                //uncomment this
                $scope.isLinkVisible.clientRetention = true;
                $scope.isLinkVisible.actualVsProjPipelineRev = true;
                
                //trigger restriction on routechange
                $scope.isLinkVisible.isLoaded = true;

                //set homepage
                if (authService.authUser.isManager && ($location.path() === '/login' || IfPageIsRestricted())) {
                    $location.path('/');//manager homepage
                } else if ($location.path() === '/login' || IfPageIsRestricted()) {
                    $location.path('/companyDirectory');//employee homepage
                }

            });
        }

        $scope.logOut = function () {
            authService.logOut();
            window.close();
            $location.path('/blank');
        }


        $rootScope.$on('$routeChangeStart', function (event) {
            authService.fillAuth();

            if ($location.path() === '/login') {
                authService.logOut();
                $scope.user = '';
                $scope.isManager = false;
            } else if ($scope.user != authService.authUser.firstName) {
                $scope.user = authService.authUser.firstName;
                $scope.isManager = authService.authUser.isManager;
                AccessControl();
            }

            if (!authService.authUser.isAuth && !($location.path() === '/login' || $location.path() === '/blank' || $location.path() === '/login/')) {
                $location.path('/blank');
            } else if (authService.authUser.isAuth && $location.path() === '/blank') {
                $location.path('/dashboard');
            } else if (IfPageIsRestricted() && $scope.isLinkVisible.isLoaded) {
                event.preventDefault();
            }

            //selected page header
            if ($location.path() === '/') {
                $scope.selectedpagehere = 'Dashboard';
                $scope.selectedPageDropDown = '';
            }
            else if ($location.path() === '/companyDirectory') {
                $scope.selectedpagehere = 'Company Directory';
                $scope.selectedPageDropDown = 'Human Resources'
            }
            else if ($location.path() === '/employeeProfile') {
                $scope.selectedpagehere = 'Employee Profile';
                $scope.selectedPageDropDown = 'Human Resources'
            }
            else if ($location.path() === '/calendar') {
                $scope.selectedpagehere = 'Calendar';
                $scope.selectedPageDropDown = 'Human Resources';
            }
            else if ($location.path() === '/boardStatistics') {
                $scope.selectedpagehere = 'Board Statistics';
                $scope.selectedPageDropDown = 'Board Report';
            }
            else if ($location.path() === '/monthlyClearing') {
                $scope.selectedpagehere = 'Monthly Clearing';
                $scope.selectedPageDropDown = 'Board Report';
            }
            else if ($location.path() === '/salesCommission') {
                $scope.selectedpagehere = 'Sales Commission';
                $scope.selectedPageDropDown = 'Board Report';
            }
            else if ($location.path() === '/clientRetention') {
                $scope.selectedpagehere = 'Client Retention';
                $scope.selectedPageDropDown = 'Board Report';
            }
            else if ($location.path() === '/actualVsProjPipelineRev') {
                $scope.selectedpagehere = 'Actual vs. Proj. Pipeline Rev.';
                $scope.selectedPageDropDown = 'Board Report';
            }
            else if ($location.path() === '/volumesAndChargesStats') {
                $scope.selectedpagehere = 'Volumes And Charges Stats';
                $scope.selectedPageDropDown = 'Board Report';
            }
            else if ($location.path() === '/clearingFeePerAccount') {
                $scope.selectedpagehere = 'Clearing Fee Per Account';
                $scope.selectedPageDropDown = 'Internal Control';
            }
            else if ($location.path() === '/monthlyClearingValidation') {
                $scope.selectedpagehere = 'Monthly Clearing Validation';
                $scope.selectedPageDropDown = 'Internal Control';
            }
            else if ($location.path() === '/yearlyRevenueAndSale') {
                $scope.selectedpagehere = 'Yearly Revenue And Sale';
                $scope.selectedPageDropDown = 'Internal Control';
            }
        });

        var IfPageIsRestricted = function () {
            if ($location.path() === '/' && !$scope.isLinkVisible.dashBoard) {
                return true;
            } else if ($location.path() === '/yearlyRevenueAndSale' && !$scope.isLinkVisible.yearlyRevenueAndShare) {
                return true;
            } else if ($location.path() === '/clearingFeePerAccount' && !$scope.isLinkVisible.clearingFeePerAccount) {
                return true;
            } else if ($location.path() === '/salesCommission' && !$scope.isLinkVisible.salesCommission) {
                return true;
            } else if ($location.path() === '/companyDirectory' && !$scope.isLinkVisible.companyDirectory) {
                return true;
            } else if ($location.path() === '/volumesAndChargesStats' && !$scope.isLinkVisible.volumesAndChargesStats) {
                return true;
            } else if ($location.path() === '/employeeProfile' && !$scope.isLinkVisible.employeeProfile) {
                return true;
            } else if ($location.path() === '/monthlyClearing' && !$scope.isLinkVisible.monthlyClearing) {
                return true;
            } else if ($location.path() === '/calendar' && !$scope.isLinkVisible.calendar) {
                return true;
            } else if ($location.path() === '/boardStatistics' && !$scope.isLinkVisible.boardStatistics) {
                return true;
            } else if ($location.path() === '/monthlyClearingValidation' && !$scope.isLinkVisible.monthlyClearingValidation) {
                return true;
            } else if ($location.path() === '/clientRetention' && !$scope.isLinkVisible.clientRetention) {
                return true;
            } else if ($location.path() === '/actualVsProjPipelineRev' && !$scope.isLinkVisible.actualVsProjPipelineRev) {
                return true;
            }
            return false;
        }

    }]);
