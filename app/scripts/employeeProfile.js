﻿'use strict';
app.controller('employeeProfileController', ['$scope', 'authService', 'http', '$modal', function ($scope, authService, http, $modal) {

    $scope.department = { Name: 'All' };
    $scope.status = true;
    $scope.tableData = [];
    $scope.columns = [{ name: "Name", ticked: true },
                { name: "Company Name", ticked: true },
                { name: "Title", ticked: true },
                { name: "Division", ticked: true },
                { name: "Department", ticked: true },
                { name: "Cell Phone", ticked: true },
                { name: "Home Phone", ticked: true },
                { name: "AIM ID", ticked: true },
                { name: "Employment Status", ticked: true },
                { name: "Payment Type", ticked: true }
    ];


    http.post('GetDepartments', '').success(function (results) {
        $scope.departments = JSON.parse($(results).attr('d'));
    });

    $scope.LoadTable = function () {
        http.post('GetEmployees', { Department: $scope.department.Name, Status: $scope.status }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10 };
        });
    }
    $scope.LoadTable();

    //sorting
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.predicate = 'Name';
    $scope.reverse = false;


    var modal = $modal({ scope: $scope, templateUrl: 'app/views/modalEmployee.html', show: false, backdrop: 'static', controller: 'modalEmployeeController' });
    $scope.showModal = function (id,title) {
        $scope.employeeID = id;
        $scope.title = title;
        $scope.CloseModal = function () {
            $scope.LoadTable();
            modal.hide();
        }
        modal.$promise.then(modal.show);

    };
}]);





