﻿var app = angular.module('green', ['mgcrea.ngStrap', 'ngRoute', 'ngAnimate', 'LocalStorageModule', 'bw.paging', 'isteven-multi-select', 'textAngular', 'chart.js', 'angular-intro']);

app.config(['$routeProvider', function ($routeProvider) {


    $routeProvider.
    when('/login', {
        templateUrl: 'APP/Views/login.html',
        controller: 'loginController'
    }).
    when('/blank', {
        templateUrl: 'APP/Views/blank.html'
    }).
    when('/companyDirectory', {
        templateUrl: 'APP/Views/companyDirectory.html',
        controller: 'companyDirectoryController'
    }).
    when('/employeeProfile', {
        templateUrl: 'APP/Views/employeeProfile.html',
        controller: 'employeeProfileController'
    }).
    when('/calendar', {
        templateUrl: 'APP/Views/calendar.html',
        controller: 'calendarController'
    }).
    when('/boardStatistics', {
        templateUrl: 'APP/Views/boardStatistics.html',
        controller: 'boardstatisticsController'
    }).
    when('/monthlyClearing', {
        templateUrl: 'APP/Views/monthlyClearing.html',
        controller: 'monthlyClearingController'
    }).
    when('/salesCommission', {
        templateUrl: 'APP/Views/salesCommission.html',
        controller: 'salesCommissionController'
    }).
    when('/volumesAndChargesStats', {
        templateUrl: 'APP/Views/volumesAndChargesStats.html',
        controller: 'volumesAndChargesStatsController'
    }).
    when('/clearingFeePerAccount', {
        templateUrl: 'APP/Views/clearingFeePerAccount.html',
        controller: 'clearingFeePerAccountController'
    }).
    when('/monthlyClearingValidation', {
        templateUrl: 'APP/Views/monthlyClearingValidation.html',
        controller: 'monthlyClearingValidationController'
    }).
    when('/yearlyRevenueAndSale', {
        templateUrl: 'APP/Views/yearlyRevenueAndSale.html',
        controller: 'yearlyRevenueAndSaleController'
    }).
    when('/clientRetention', {
        templateUrl: 'APP/Views/clientRetention.html',
        controller: 'clientRetentionController'
    }).
    when('/actualVsProjPipelineRev', {
        templateUrl: 'APP/Views/actualVsProjPipelineRev.html',
        controller: 'actualVsProjPipelineRev'
    }).
    when('/', {
        templateUrl: 'APP/Views/dashboard.html',
        controller: 'dashboardController'
    }).
    otherwise({
        redirectTo: '/'
    });
}]);

app.constant('appSettings', {
    serviceBase: 'ClientWebService.asmx/',
    converterBase: 'http://192.168.10.2:9090/ca/converters'
});

Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}


//used in column search item
app.directive('deleteIfEmpty', function () {
    return {
        restrict: 'A',
        scope: {
            ngModel: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch("ngModel", function (newValue, oldValue) {
                if (typeof scope.ngModel !== 'undefined' && scope.ngModel.length === 0) {
                    delete scope.ngModel;
                }
            });
        }
    };
});

//customer filter for paging
app.filter('startFrom', function () {
    return function (input, start) {
        if (!input || !input.length) { return; }
        start = +start; //parse to int
        return input.slice(start);
    }
});


function tour() {
    introJs().refresh();

    var introJsStarted = introJs().setOptions({
        doneLabel: 'Done',
        showStepNumbers: false,
        disableInteraction: true,
        scrollToElement: true
    }).start()

    introJsStarted
    .oncomplete(function () {
        introJs().exit()
    }).onexit(function () {
        introJs().exit()
    }).onchange(function (targetElement) {

    })
}
