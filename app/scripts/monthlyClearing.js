﻿'use strict';
app.controller('monthlyClearingController', ['$scope', 'authService', 'http', 'dropDownService', function ($scope, authService, http, dropDownService) {
   
    $scope.year = { Name: new Date().toJSON().slice(0, 4) };
    $scope.months = dropDownService.getMonths();
    $scope.month = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth() - 1)];
    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    $scope.tableData = [];

    $scope.LoadTable = function () {
        http.post('GetMonthlyClearing', { Month: $scope.month.Name, Year: $scope.year.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
        });
    }
    $scope.LoadTable();



    //set css for headers
    $scope.isIncludedInList = function (value, value2) {
        if (value && !value2) {
            return true;
        } 
        return false;
    }

    //set css for headers titles
    $scope.isIncludedInList2 = function (value) {
        if (value === "Account" || value === "Month" || value === "Day" || value === "Venue" || value === "Transaction Type") {
            return true;
        } else if (value === "Total") {
            return true;
        }
        return false;
    }

}]);


