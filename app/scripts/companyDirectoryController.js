﻿'use strict';
app.controller('companyDirectoryController', ['$scope', 'authService', 'http', 'downloadService', function ($scope, authService, http, downloadService) {
    $scope.activePanels = [];
    $scope.btnDownload = 'Download';
    //$scope.activePanel = 1;
    $scope.department = { Name: 'All' };
    $scope.tableData = [];
    $scope.columns = [{ name: "Name", ticked: true },
                { name: "Company Name", ticked: true },
                { name: "Title", ticked: true },
                { name: "Division", ticked: true },
                { name: "Department", ticked: true },
                { name: "Cell Phone", ticked: true },
                { name: "Work Phone", ticked: true },
                { name: "AIM ID", ticked: true },
                { name: "Skype ID", ticked: true },
                { name: "Email Add", ticked: true }
    ];

    http.post('GetDepartments', '').success(function (results) {
        $scope.departments = JSON.parse($(results).attr('d'));
    });


    $scope.LoadTable = function () {
        http.post('GetCompanyDirectory', { Department: $scope.department.Name === 'All' ? '' : $scope.department.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10 };
        });
    }
    $scope.LoadTable();

    //sorting
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };


    $scope.DownloadCompanyDirectory = function () {
        if ($scope.btnDownload === 'Download') {
            $scope.btnDownload = 'Downloading..';
            var table = document.getElementById('tableParent').innerHTML;
            var image = '';
            var legends = '';
            var reportParameters = document.getElementById('reportParameters').innerHTML;
            var latestData = '';
            var otherOptions = { orientation: 'landscape' };
            var topHeader = document.getElementById('topHeader').innerHTML;

            table = topHeader + table;

            downloadService.pdf(table, image, legends, reportParameters, latestData, 'Company Directory', otherOptions).then(function (res) {
                if (res) {
                    $scope.btnDownload = 'Download';
                }
            });
        }
    }


    $scope.predicate = 'Name';
    $scope.reverse = false;



}]);





