﻿'use strict';
app.controller('modalEmployeeController', ['$scope', 'http', 'authService', '$alert', function ($scope, http, authService, $alert) {
    $scope.employee = [];
    $scope.employee.Status = 'Active';
    $scope.employee.Manager = { Name: '' };
    $scope.employee.Division = { Name: '' };
    $scope.employee.Department = { Name: '' };
    $scope.employee.CompanyName = { Name: '' };
    $scope.employee.EmploymentStatus = { Name: '' };

    //dropdown lists
    http.post('GetEmployeeNames', '').success(function (results) {
        $scope.employeeNames = JSON.parse($(results).attr('d'));
    });

    http.post('GetDivisions', '').success(function (results) {
        $scope.divisions = JSON.parse($(results).attr('d'));
    });

    http.post('GetDepartments', '').success(function (results) {
        $scope.departments = JSON.parse($(results).attr('d'));
    });

    http.post('GetEmployeeCompanies', '').success(function (results) {
        $scope.companies = JSON.parse($(results).attr('d'));
    });
    http.post('GetEmploymentStatuses', '').success(function (results) {
        $scope.employmentStatuses = JSON.parse($(results).attr('d'));
    });



    //add or edit view
    if ($scope.employeeID) {
        http.post('GetEmployee', { EmployeeID: $scope.employeeID }).success(function (results) {
            $scope.employee = JSON.parse($(results).attr('d'))[0];
            //get values for dropdownlists model
            $scope.employee.Manager = { Name: $scope.employee.Manager };
            $scope.employee.Division = { Name: $scope.employee.Division };
            $scope.employee.Department = { Name: $scope.employee.Department };
            $scope.employee.CompanyName = { Name: $scope.employee.CompanyName };
            $scope.employee.EmploymentStatus = { Name: $scope.employee.EmploymentStatus };
        });
    }

    $scope.Save = function () {
        http.post('AddOrEditEmployee', {
            EmployeeID: $scope.employeeID,
            Division: $scope.employee.Division.Name,
            Department: $scope.employee.Department.Name,
            FirstName: $scope.employee.FirstName ? $scope.employee.FirstName : '',
            LastName: $scope.employee.LastName ? $scope.employee.LastName : '',
            Title: $scope.employee.Title ? $scope.employee.Title : '',
            CellPhone: $scope.employee.CellPhone ? $scope.employee.CellPhone : '',
            WorkPhone: $scope.employee.WorkPhone ? $scope.employee.WorkPhone : '',
            HomePhone: $scope.employee.HomePhone ? $scope.employee.HomePhone : '',
            EmailAdd: $scope.employee.EmailAdd ? $scope.employee.EmailAdd : '',
            AltEmailAdd: $scope.employee.AltEmailAdd ? $scope.employee.AltEmailAdd : '',
            AIMID: $scope.employee.AIMID ? $scope.employee.AIMID : '',
            Manager: $scope.employee.Manager.Name,
            Status: $scope.employee.Status ? $scope.employee.Status : '',
            CompanyName: $scope.employee.CompanyName.Name,
            EmploymentStatus: $scope.employee.EmploymentStatus.Name,
            DivisionManager: $scope.employee.DivisionManager ? $scope.employee.DivisionManager : false,
            ModifiedBy: authService.authUser.token,
            SkypeID: $scope.employee.SkypeID ? $scope.employee.SkypeID : '',
            HireDate: $scope.employee.HireDate ? new Date($scope.employee.HireDate) : new Date('01-01-1900'),
        }).success(function (results) {
            $scope.message = '';
            if (JSON.parse($(results).attr('d'))[0].MSG === 'Employee Profile already exists') {
                $scope.message = JSON.parse($(results).attr('d'))[0].MSG
            } else  {
                $scope.CloseModal();
                $alert({
                    title: JSON.parse($(results).attr('d'))[0].MSG ,
                    content: '&nbsp',
                    placement: 'top-rightModalAlert',
                    type: 'info',
                    show: true,
                    duration: 3
                });
            }
        });
    }

    $scope.Delete = function (employeeID) {
        http.post('DeleteEmployee', { EmployeeID: employeeID }).success(function (results) {
            $scope.CloseModal();
            $alert({
                title: 'Employee was deleted',
                content: '&nbsp',
                placement: 'top-rightModalAlert',
                type: 'info',
                show: true,
                duration: 3
            });

        });
    }

    $scope.IntroOptionsEmployeeProfile = {
        steps: [
       {
           element: "#one",
           intro: "Complete this form to add/edit an Employee.",
           position: 'right'
       },
       {
           element: "#two",
           intro: "Click here to Save.",
           position: 'right'
       }

        ],
        showStepNumbers: false,
        disableInteraction: true,
        exitOnOverlayClick: true,
        exitOnEsc: true,
        scrollToElement: true


    };


        

}]);





