﻿app.controller('actualVsProjPipelineRev', ['$scope', 'http', 'dropDownService', 'downloadService', function ($scope, http, dropDownService, downloadService) {
    document.getElementById("btnDownload").style.visibility = "hidden";
    $scope.options = {
        onAnimationComplete: function () {
            document.getElementById("btnDownload").style.visibility = "visible";
        }
    }

    $scope.ddCompanies = [];
    $scope.selectedCompanies = [];
    var _selectedCompanies = [];
    $scope.unit = {};
    $scope.unit.value = '';
    $scope.series = ['Actual Rev.', 'Proj. Pipeline Rev.'];
    $scope.chartColors = ['#008000', '#DEB887'];
    $scope.scaleLabel = ['#008000', '#DEB887'];
    $scope.latestData = [];
    $scope.tableData = [];
    var _chartData = [];


    $scope.fromMonth = moment().subtract(12, 'months');
    $scope.activePanels = [0];
    $scope.months = dropDownService.getMonths();
    $scope.toYear = { Name: new Date().toJSON().slice(0, 4) };
    $scope.fromYear = { Name: new Date().toJSON().slice(0, 4) - 1 };
    $scope.fromMonth = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth())];
    $scope.toMonth = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth())];

    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    $scope.downloadText = 'Download';
    $scope.downloadPDF = function () {
        if ($scope.downloadText === 'Download') {
            $scope.downloadText = 'Downloading..';
            var table = document.getElementById('tableParent').innerHTML;
            var image = document.getElementById("chartImage").toDataURL();
            var legends = document.getElementById("legends").innerHTML;
            var reportParameters = document.getElementById("reportParameters").innerHTML;
            var latestData = document.getElementById("latestData").innerHTML;

            downloadService.pdf(table, image, legends, reportParameters, latestData, 'Actual vs. Proj. Pipeline Rev.').then(function (res) {
                if (res) {
                    $scope.downloadText = 'Download';
                }
            });
        }

    }

    http.post('ActualVsProjPipelineRevUnits', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name) }).success(function (results) {
        $scope.units = JSON.parse($(results).attr('d'));
        $scope.unit = $scope.units[0];
        $scope.ChangeView();
        $scope.ChangeDates();
    });

    var view = 'Chart';
    $scope.ChangeView = function () {
        if (view === 'Chart') {
            $scope.activePanels = [0];
            view = 'Table';
        } else {
            $scope.activePanels = [1];
            view = 'Chart';
        }
    }

    $scope.ChangeDates = function () {
        document.getElementById("btnDownload").style.visibility = "hidden";
        //dropdown selection
        http.post('ActualVsProjPipelineCompanyName', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name) }).success(function (results) {
            $scope.ddCompanies = JSON.parse($(results).attr('d'));
            $scope.selectedCompanies = [];

            for (var i = 0; i < $scope.ddCompanies.length; i++) {
                $scope.selectedCompanies.push($scope.ddCompanies[i].value);
            }
            _selectedCompanies = angular.copy($scope.selectedCompanies);
        });

        //table
        http.post('ActualVsProjPipelineRevTable', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name), Unit: $scope.unit.value }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
            $scope.page = { currentPage: 1, total: $scope.tableData.length, size: 10 };
        });

        //chart
        $scope.labels = [];
        $scope.data = [[], []];

        http.post('ActualVsProjPipelineRevChart', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name), Unit: $scope.unit.value }).success(function (results) {
            _chartData = JSON.parse($(results).attr('d'));

            for (var i = 0; i < _chartData.length; i++) {
                if ($scope.labels.indexOf(_chartData[i].Date) === -1) {
                    $scope.labels.push(_chartData[i].Date);
                }
                $scope.data[0].push(_chartData[i]['ActualRev'].toFixed(2));
                $scope.data[1].push(_chartData[i]['ProjPipelineRev'].toFixed(2));
            }
            // to get the latest data for report
            $scope.latestData = [];

            for (var i = 0; i < $scope.series.length; i++) {
                $scope.latestData[i] = { value: $scope.data[i][$scope.data[i].length - 1], field: $scope.series[i], color: $scope.chartColors[i] };
            }
        });
    }


    $scope.selectDropDownCompanies = function (param) {
        if (param === 'All') {
            $scope.selectedCompanies = angular.copy(_selectedCompanies);
        } else {
            $scope.selectedCompanies = [];
        }
    }
    //sorting
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };
    $scope.predicate = '';
    $scope.reverse = false;
    $scope.customFilter = function (item) {
        if ($scope.selectedCompanies.indexOf(item.CompanyName) != -1) {
            return item;
        }
    };

}]);

