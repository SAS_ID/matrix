﻿'use strict';
app.controller('boardstatisticsController', ['$scope', 'http', 'dropDownService', 'downloadService', function ($scope, http, dropDownService, downloadService) {
    $scope.unit = { Name: 'ETCGG - ETC Global Group, LLC' };
    $scope.year = { Name: new Date().toJSON().slice(0, 4) };
    $scope.months = dropDownService.getMonths();
    $scope.month = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth() - 1)];
    $scope.units = dropDownService.getUnits();
    $scope.lblProcess = 'Process';


    $scope.tableData = [];
    $scope.columns = [{ name: "Period (Month - Year)" },
                { name: "Unit" },
                { name: "No. of Clients" },
                { name: "% of Clients" },
                { name: "Revenue" },
                { name: "% Revenue" },
                { name: "Eq. Volume" },
                { name: "Eq. % Volume" },
                { name: "Eq. Market Volume" },
                { name: "Eq. ETC % Volume" },
                { name: "Opt. Volume" },
                { name: "Opt. ETC % Volume" },
                { name: "Opt. Market Volume" },
                { name: "Opt. % Volume" }
    ];



    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    $scope.LoadTable = function () {
        http.post('GetBoardStatistics', { Month: $scope.month.Name, Year: $scope.year.Name, Unit: $scope.unit.Name }).success(function (results) {
            $scope.tableData = JSON.parse($(results).attr('d'));
    });
}
    $scope.LoadTable();
$scope.ProcessBoardStatistics = function () {
    $scope.lblProcess = 'Processing..';
    http.post('ProcessBoardStatistics', { Month: $scope.month.Name, Year: $scope.year.Name, Unit: $scope.unit.Name }).success(function (results) {
        $scope.lblProcess = 'Process';
    });
}

$scope.downloadText = 'Download';
$scope.downloadPDF = function () {
    if ($scope.downloadText === 'Download') {
        $scope.downloadText = 'Downloading..';
        var table = document.getElementById('tableParent').innerHTML;
        var image = '';
        var legends = '';
        var reportParameters = document.getElementById("reportParameters").innerHTML;
        var latestData = '';
        var otherOptions = { orientation: 'landscape' };

        downloadService.pdf(table, image, legends, reportParameters, latestData, 'Board Statistics', otherOptions).then(function (res) {
            if (res) {
                $scope.downloadText = 'Download';
            }
        });
    }
};

//sorting
$scope.order = function (predicate) {
    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
    $scope.predicate = predicate;
};

//set css for headers
$scope.isIncludedInList = function (value) {
    if (value === 'Black Box' || value === 'Prof. Trading Firm' || value === 'Others' || value === 'Total Statistics') {
        return true;
    }
    return false;
}

//set css total bold style
$scope.isIncludedInList2 = function (value) {
    if (value === "Total") {
        return true;
    }
    return false;
}
$scope.predicate = 'Name';
$scope.reverse = false;
}]);





