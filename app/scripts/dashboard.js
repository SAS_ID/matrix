﻿app.controller('dashboardController', ['$scope', 'http', 'dropDownService', function ($scope, http, dropDownService) {

    //var pdf = [];
    $scope.series = ['Clearing Revenue', 'Share Count'];
    $scope.chartColors = ['#008000', '#DEB887'];
    $scope.chatClearingTable = [];

    $scope.fromMonth = moment().subtract(12, 'months');
    $scope.activePanels = [0];
    $scope.months = dropDownService.getMonths();
    $scope.toYear = { Name: new Date().toJSON().slice(0, 4) };
    $scope.fromYear = { Name: new Date().toJSON().slice(0, 4) - 1 };
    $scope.fromMonth = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth())];
    $scope.toMonth = $scope.months[(new Date().getMonth() === 0 ? 0 : new Date().getMonth())];

    http.post('GetYears', '').success(function (results) {
        $scope.years = JSON.parse($(results).attr('d'));
    });

    var view = 'Chart';
    $scope.ChangeView = function () {
        if (view === 'Chart') {
            $scope.activePanels = [0];
            view = 'Table';
        } else {
            $scope.activePanels = [1];
            view = 'Chart';
        }

    }
    $scope.ChangeView();


    //$scope.downloadPDF = function () {
    //    http.post('GetDate', '').success(function (results) {
    //        pdf.legends = [{ text: 'Clearing Revenue', color: '#008000' }, { text: 'Share Count', color: '#DEB887' }];
    //        pdf.tables = [];
    //        pdf.tables[0] = [];
    //        pdf.tables[0].headers = ['Date', 'Share Count', 'Clearing Revenue'];
    //        pdf.tables[0].body = $scope.chatClearingTable;
    //        pdf.chart = document.getElementById("chartImage").toDataURL();

    //        pdf.title = 'Clearing Revenue (' + $scope.fromMonth.Name + ' ' + $scope.fromYear.Name + ' - ' + $scope.toMonth.Name + ' ' + $scope.toYear.Name + ') ' + results['d'];
    //        downloadService.chartAndTables(pdf);
    //    });
    //}

    $scope.ChangeDates = function () {
        $scope.labels = [];
        $scope.data = [[], []];
        http.post('GetMonthlyChart', { StartDate: new Date($scope.fromMonth.Name + ' ' + $scope.fromYear.Name), EndDate: new Date($scope.toMonth.Name + ' ' + $scope.toYear.Name) }).success(function (results) {
            $scope.chatClearingTable = JSON.parse($(results).attr('d')).sort(function (a, b) {
                return new Date(a.Date).getTime() - new Date(b.Date).getTime()
            });

            var chatClearingRevenue = [];
            var shareCount = [];
            //total used to compute percentage
            var totalRevenue = 0;
            var totalshareCount = 0;

            for (var i = 0; i < $scope.chatClearingTable.length; i++) {
                $scope.labels.push($scope.chatClearingTable[i]['Date']);
                chatClearingRevenue.push($scope.chatClearingTable[i]['ChatClearingRevenue']);
                shareCount.push($scope.chatClearingTable[i]['ShareCount']);

                totalRevenue += $scope.chatClearingTable[i]['ChatClearingRevenue'];
                totalshareCount += $scope.chatClearingTable[i]['ShareCount'];
            }

            for (var i = 0; i < $scope.chatClearingTable.length; i++) {
                $scope.data[0].push(((chatClearingRevenue[i] / totalRevenue) * 100).toFixed(2));
                $scope.data[1].push(((shareCount[i] / totalshareCount) * 100).toFixed(2));
            }
        });


    }
    $scope.ChangeDates();

}]);

